# Changelog

## 2024-05-06 (Current) - Mise à jour des petits panneaux nom_station
### TCH :
 - Le fichier `nom_station/nom_station.svg`, contenant les petites versions des panneaux de station TCH, a été mis à jour (toutes les stations ajoutées ces derniers mois y figurent désormais, et tout a été retrié par ordre alphabétique).

## 2024-03-27 (c82bb08d) - Mise à jour des tracés du RER, du métro, et de certaines villes (#33 #34)
### Carte interactive :
 - Retraçage de plusieurs zones selon le récent déplacement de Grenat : la mer d’Etherium, les îles de Grenat / Récif / Linbeck, quelques îles mineures alentour, la tourbière de Braâhm, la rade de Niascar.
 - Retraçage des limites de plusieurs villes : Grenat, Asvaard, Cyséal, Port-aux-Pendus, Port-Putride, Thomorgne, Triel, Chaugnes, Chouigny, Traverzy, Touissy, Fouizy-la-Décharge, Sommieu, Chalandières, Floutrias, Barbourrigues, Gléry, Vénissy.
 - Renommage de toutes les occurences de Venise / VillageCS en Vénissy.
 - Ajout des stations Grenat–Port et Le Hameau–Port. (#33, #34)
 - Retraçage dans `index.html` de toutes les voies de métro / RER nouvellement construites ou dont le tracé a été récemment modifié.

## 2024-03-25 (8d9833c1) - Correction des liens du CGC (#26)
### Technique :
 - Les liens du CGC générés dans `map.js` sont désormais corrects et renvoient vers l’article s’il existe.

## 2024-03-25 (a2f8827d) - Mise à jour des cartes (mai 81)
### Fond de carte :
 - Mise à jour des cartes de l’ensemble du territoire (avril-mai 81).

## 2024-03-25 (f90e3db7) - Mise à jour des timers PID
### TCH :
 - Mise à jour des timers du PID (`pid/timer.js`) : le décompte s’arrête désormais à 60 au lieu de 99, et quelques nouveaux caractères et sigles ont été ajoutés (**++**, **..** et **XX**).

## 2024-03-19 (413edbb1) - Ajout des noms définitifs des villes nouvellement ajoutées
### TCH :
 - Dans `index.html`, toutes les villes ayant des noms temporaires (`XX`…) mais un nom définitif dans `plan_reseau/complet_fr.svg` ont reçu leur nom définitif.

## 2023-04-06 (1ad72736) - Nouveaux PID et acces_quais pour le RER A
### TCH :
 - Les PID et accès aux quais du RER A, qui étaient les derniers à ne pas avoir été mis à jour avec les changements récents (nommage numérique plutôt que textuel pour les noms des fichiers/IDs des éléments, ajout et nommage des nouvelles gares et stations).
 - Les PID du M12 incluent désormais à nouveau des directions uniques pour les directions 122 et 123 (depuis le dernier commit on avait uniquement la version double, ce qui posait problème quand on était *sur* l'une des branches concernées).
 - Le style des PID (2x1 comme 3x1), qui est embed dans les fichiers plutôt que séparé, a (enfin) reçu une correction qui lui permet d'afficher correctement le gras lorsqu'il est prévisualisé dans un navigateur Web (le "cheat" qu'on utilisait jusqu'à présent était exclusif à Inkscape, ce qui est un peu con).

## 2023-03-19 (4c130f2a) - Nommage définitif des terminus des lignes 9 à 12
### TCH :
 - Les fichiers `pid/2x1.svg`, `corresp/corresp.svg`, `acces_quais/2x1.svg` et `/3x1.svg` ont désormais les noms définitifs des terminus des lignes de métro 9 à 12.
 - On a réduit la taille de certains noms de terminus sur les PID 2x1 (`pid/2x1.svg`) pour permettre d'afficher correctement les SACD ingame face à leur terminus. On a également condensé les 2 PID `m12_122` et `m12_123` en un groupe unique `m12_122_123`, qui contient les 2 terminus sur 2 lignes différentes.

## 2023-03-19 (a21cab92) - Correction de la direction de sections de la branche A1 du RER A
### TCH :
 - Les sections mono-directionnelles desservant Chaugnes et Triel ont vu leur direction corrigée ; elles étaient inversées sur la version précédente de la carte.
 - Deux versions des flèches mono-directionnelles ont été ajoutées pour remplacer `icon/dir_ligne.svg` : `icon/dir_ligne_g.svg` et `dir_ligne_d.svg`.

## 2023-03-16 (ec185666) - Plan du réseau TCH étendu au nord
### TCH :
 - Le plan du réseau étendu est désormais disponible dans `plan_reseau/complet_fr.svg`. Il se base sur le style du plan d'origine `metro_plan.svg`, et sa feuille de style `plan_reseau/plan_reseau.css` est quasi-identique à `metro_plan.css`. Il inclut les nouveaux tracés des lignes 9 à 12, du RER A3/A4, les nouvelles stations des lignes existantes, ainsi que les lignes de bateau prévues en surimpression grise.
 - Toutes les icônes de terminus métro du nouveau plan du réseau sont désormais similaires à celles du RER, avec un point coloré de la couleur de la ligne à l'intérieur du cercle. De nouvelles icônes ont été ajoutées à cet effet dans `station/`, sous les noms `t<ID>.svg` (par exemple `t5a.svg` pour le M5a ou `t9` pour le M9).
 - De nouvelles icônes de station ont été ajoutées pour les correspondances métro/bateau sur le nouveau plan du réseau. Elles sont nommées `station/<ID>_b.svg`, mais seules celles utilisées par le plan du réseau ont été créées (par exemple `station/5b_b.svg` pour la ligne de métro 5b).
 - Trois nouvelles icônes de station avec correspondance ont été créées dans `station/`, respectivement pour Grenat–Ville (`x_Xr_1.svg`), Villonne (`x_Xr_5b.svg`) et Chassy-sur-Flumine (`Xr_7.svg`).
 - Une icône de station dans le style "lieu-dit" a été ajoutée dans `station/ld.svg` et est utilisée sur le plan du réseau étendu pour marquer les stations de bateau uniquement.
 - Une icône de demi-flèche servant à indiquer le sens d'une ligne mono-directionnelle (et utilisée par la branche A1 du RER A sur le plan du réseau étendu) a été ajoutée dans `icon/dir_ligne.svg`.

## 2023-03-12 (f64300db) - Nouveaux tracés métro et RER pour les panneaux TCH
### TCH :
 - Les panneaux `acces_quais`, `corresp`, `pid/2x1.svg`, ainsi que les plans `totem`, ont été mis à jour pour intégrer les lignes de métro nouvellement ajoutées (M9/10/11/12), ainsi que les nouveaux tracés et terminus des lignes de métro 1, 2, 3, 4, 5a et 7, ainsi que du RER A.
 - Tous les fichiers indiqués ci-dessus ont désormais des ID correspondant à leurs nouveaux ID ingame, pour le batch export : ils n'utilisent plus que les ID numériques des lignes et des stations, afin qu'ils ne changent pas lorsqu'on change les tracés comme actuellement.
 - Ajout du logo de mode de transport de bateau, en version fond blanc (`logo/b.svg`) ou transparent (`logo/b_transp.svg`).
 - Ajout du fichier `totem/totem_bg.svg` ; il s'agit de la face arrière des totems, car ils ont désormais un modèle custom dans le RP pour éviter d'afficher leur texte sur la face arrière.

## 2023-02-20 (c48678e4/c71daeb6) - Mise à jour des cartes topographiques
### Fond de carte :
 - Diverses parties manquantes du calque topographique ont été ajoutées.
 - Un script (*topo/tile_topo.bat*) permet de générer une image complète à partir des différentes tiles de la carte topographique.

## 2023-01-07 (0ad2018b/0f9714f0/2d02ecbf/90c266ed/00848f2f) - Nouvelle version web
### Web :
 - Intégration de `map.svg` dans `index.html`
 - Ajout d'assets pour la version web
 - Changement des liens vers le wiki pour `https://cgc.terresduhameau.eu.org`

## 2022-08-07 (a3cd8767) - Ajout d'une ébauche de carte topographique (commit de travail)
### Fond de carte :
 - Un calque "Topographie" a été ajouté et est activable/désactivable dans la légende. Il y a comme pour le fond de carte différentes résolutions pour les tiles, mais la résolution max est de 512 (au lieu de 1024) car elles sont alignées sur les fichiers region. Le script permettant de générer les différentes résolutions est disponible dans *topo/gen_tiles.bat*.
 - Quelques ajustements divers ont été faits dans *map.js* : dans *keyUp(e)*, les touches ont été remises par ordre alphabétique, et le chargement (dans les divers *onloadX*) intègre les cartes topographiques.

## 2022-08-05 (15b4adfa) - Retraçage des aides de sélection des lignes de métro (commit de travail)
### TCH :
 - Les aides à la sélection (tracés invisibles des lignes de transport plus larges que celles-ci et servant à les sélectionner plus facilement) ont été retracés et ajustés du mieux possible, y compris pour les lignes de bateau et de câble.
 - Les lignes de RER sont désormais affichées derrière les lignes de métro, de câble et de bateau.
 - Les logos terminus du RER A indiquent désormais les ID de branche, et les logos de branche ne les indiquent plus.

## 2022-08-04 (67e97583) - Projets bateau et logos terminus (commit de travail)
### TCH :
 - Ajout de nombreuses lignes de bateaux omises dans le commit précédent.
 - Ajout des logos des modes de transport au terminus de toutes les lignes. Les terminus en projet (non encore desservis) sont cerclés en noir et jaune, sauf pour les bateaux.
 - Les stations/lignes (dans les métadonnées des stations et des lignes) séparent désormais les connexions existantes (dans "stations" ou "lignes") et les connexions en projet (dans "stations-projet" ou "lignes-projet").
 - Les lignes stockent désormais un tag supplémentaire, "id-ligne", pour l'affichage du nom de la ligne dans l'interface textuelle (par exemple "M3" ou "RERA").
 - Toutes les gares nommées "Gare de XXX" ont été renommées "XXX (Gare)".
 - Tous les ports nommés "Port de YYY" ont été renommés "YYY (Port)".
 - Tous les ID de station de type "MetroVillage**ZZ**" ont été changés pour "Metro**ZZ**". Ce n'est pas le cas pour les RER car tout serait en majuscules et il y aurait des risques de télescopage (puisque les ID de ligne de RER sont également en lettres).
 - Les tracés invisibles non modifiés après des C/C dans le commit précédent ont été corrigés.
 - Les logos des branches de RER ont été repositionnés correctement juste après la séparation de chacune desdites branches du tronçon central, et ils utilisent désormais une variante du logo de la ligne pour ne pas jurer visuellement avec le reste de la carte.
 - Les ports sont désormais tous liés (dans le tag "related") avec les autres stations TCH des villes qu'ils desservent.

## 2022-08-02 (22c06b49) - Nouveaux projets de TCH (commit de travail)
### TCH :
 - Ajout des projets de lignes de métro 9, 10, 11 et 12.
 - Ajout des projets de modification du tracé des lignes 1 et 5a en raison du déplacement des îles de l'archipel de Grenat.
 - Ajout des projets de modification du tracé des lignes 3, 4 et 5a en raison de diverses prolongations de parcours et créations de correspondance avec le RER A.
 - Ajout de projets de tracés pour les branches 1, 3 et 4 du RER A.
 - Modification de toutes les stations et gares concernées par lesdits projets de tracés.
 - Ajout de plusieurs SVG de ports (*station/port/*).
 - Ajout de quelques lignes de bateau aux alentours du Hameau et de Grenat.

## 2022-08-01 (faf55542) - Ajout de projets pour le NE (commit de travail)
### Projets :
 - Ajouts de quelques grands projets pour le nord-est des Terres du Hameau.

## 2022-08-01 (9c0c89f6) - Màj des cartes explorées au 5 septembre 77 (commit de travail)
### Fond de carte :
 - Màj des cartes du nord-est (au 5/09/77).
### Villes et lieux-dits :
 - Ajout des villages CK à CX + AJ.
 - Ajout de nombreux potentiels lieux-dits nains.

## 2022-07-31 (d1ee8b9a) - Màj des cartes explorées au 26 août 77 (commit de travail)
### Fond de carte :
 - Ajout de plusieurs tiles de l'est des Terres du Hameau.
 - Màj des tiles autour de Chizân et de Villonne.
### Villes et lieux-dits :
 - Shikân a été renommé Le Roustiflet.
### TCH :
 - Le background de l'overlay des lignes TCH a été agrandi, car il ne recouvrait pas toute l'extension de la carte au nord du Hameau.
 - La branche A2 du RER A suit désormais son tracé réel IG, et ses gares sont correctement positionnées.
 - Deux petits fichiers svg (*station/cS_big.svg* et *c_big.svg*) ont été ajoutés pour permettre de rendre correctement le lien entre une station métro et RER quand elles sont plus éloignées que le c/cS standard (c'est le cas avec Villonne dans le positionnement ajusté).
### Projets :
 - Suppression du *Grand projet de ratiboisage océanique* en raison du reset des regions concernées.
 - Suppression des projets *Réchauffement climatique en Haute Mer de Grenat Est 3*, *Réchauffement de la forêt d'Odhur*, *Réchauffement climatique dans la baie de Thalrion*, *Suppression de l'aval (Nord-Ouest) de la rivière de la Tonnelle*, *Extension Nord de la forêt de Väam*, *Extension Est du marais de Kraken* et *Extension Ouest de la savane de Zobun* en raison de leur réalisation ingame.
 - Suppression du projet *Assèchement de la tourbière de Brâahm* dont on déterminera le sort après avoir déplacé l'île de Grenat.

## 2022-07-28 (edc67c3d) - Màj des cartes explorées au 11 juillet 77 (commit de travail)
### Fond de carte :
 - Ajout de plusieurs tiles du nord-ouest des Terres du Hameau (màj : 11/07/77).
 - Ajout d'un toggle (sans affichage dans l'interface pour l'instant...) sur la touche astérisque, qui permet de doubler la résolution des cartes affichées (désactivé par défaut).
### Villes et lieux-dits :
 - Les villages BR, BS et BT ont été ajoutés à la carte interactive.
 - De nombreux lieux-dits souterrains pouvant correspondre à de (futures) villes naines ont été ajoutés à la carte interactive. Ils portent les noms temporaires des warps leur correspondant ingame.

## 2022-07-28 (355fca64) - Màj des cartes explorées au 1er juillet 77 (commit de travail)
### Fond de carte :
 - Plusieurs tiles du nord des Terres du Hameau ont été ajoutées à la carte interactive. (màj : 1/07/77)
### Villes et lieux-dits :
 - Les villages BL à BQ ont été ajoutés à la carte interactive.

## 2022-07-26 (4418bdcf) - Réimplémentation de la résolution dynamique des tiles (commit de travail)
### Fond de carte :
 - Les tiles de 8x8 auxquelles il manquait des maps sur la ligne supérieure ont été réparées, et quelques autres explorations de *juin 77* ajoutées.
 - Les tiles sont maintenant à nouveau dans des sous-répertoires (*maps/1024*, *512*, *256*, *128* et *64*) et sont conditionnellement affichées selon le niveau de zoom de la carte. Le script de génération *maps/gen_tiles.bat* permet de générer toutes les tiles de dimensions inférieures à partir des 1024x1024.
### Villes et lieux-dits
 - Les nouveaux villages ajoutés dans le commit f8f76d03 ont vu la taille de leur point augmentée.
 - Les villages BB à BK ont été ajoutés à *map.svg*.

## 2022-07-26 (ebe6054a) - Regroupement des cartes (commit de travail)
### Fond de carte
 - Les cartes ont été regroupées en tiles de 8x8 (1024x1024).

## 2022-07-25 (f8f76d03) - Conversion 1.18.2 (commit de travail)
### Fond de carte
 - Conversion des cartes à un nouveau format plus étendu. Elles chargent pour l'instant dans un carré de 113x113 centré sur 0,0, mais les ID de carte sont réservés pour un carré jusqu'à 1999x1999.
 - Les cartes ont toutes été mises à jour en *mai 77*.
 - Le script permettant de générer le fond de carte en svg pour des dimensions arbitraires (jusqu'à 1999x1999) est disponible dans *gen/map_background.bat*.
 - Les ID de carte indiquent désormais la position du *centre* de leur carte, plutôt que du coin HG. Par conséquent du code javascript a dû être modifié dans *map.js* pour éviter que les ID affichés ne soient délirants.
 - Les cartes ont été déplacées du dossier */maps/128/* au dossier */maps/*.
### Villes et lieux-dits
 - Les villages AG, AH et AI ont été retirés de la map puisqu'on a supprimé leur morceau de carte de TDH.
 - Les nouveaux villages explorés jusqu'à VillageBA ont été rajoutés sur la carte.

## 2022-07-01 (f300395e) - rp#68 + Monuments AQ (commit de travail)
### Autres fichiers :
 - Les accès aux quais du RER A (dans *acces_quais/2x1* et *3x1.svg*) ont désormais (en principe, même si certains sont masqués car non nommés à ce jour) 2x7 versions au lieu de 2x3 : on a séparément chaque terminus, + chaque direction globale (par exemple **sud** qui contient *Procyon*+*Cyséal*), + toutes directions (dir). (rp#68)
 - Les accès aux quais incluent désormais bien les noms des monuments lorsqu'il y en a un.

## 2021-06-30 (4a87bf37) - Panneaux corresp multi-modes de transport (commit de travail)
### Autres fichiers :
 - Les panneaux multi-modes de transport (*RER A* + *M1*, *M3*, *M5a* ou *M5b*) ont été ajoutés aux panneaux corresp (*corresp/corresp.svg*). (dp#220)

## 2021-06-29 (a9a4964e) - Panneaux travaux, logo câble (commit de travail)
### TCH :
 - Les légendes des plans du réseau (*metro_plan_legende.svg*, *metro_plan_legende_nain.svg*, *metro_plan_legende_elfe.svg*) se voient désormais appliquer un clip-path, qui empêche que leurs bordures débordent.
### Autres fichiers :
 - Remplacement du logo "imagé" du câble par un C majuscule (*logo/cable*, *cable_b*, *cable_n* et *cable_transp.svg*).
 - Ajout d'une version grise du logo TCH (*logo/tch_logo_g7* et *tch_texte_g7.svg*).
 - Ajout d'un pictogramme triangulaire "attention travaux" (*logo/travaux.svg*).
 - Ajout de 3 panneaux travaux (dans *travaux/*) : *un_reseau_plus_beau*, *interdit_au_public_standalone* et *bientot_ici.svg*.
 - Les logos des lignes de câble ont désormais une taille indicative de 256x256px (au lieu de 128x128 précédemment) pour éviter des soucis de pixelisation lorsque lesdits logos sont embed dans un fichier exporté via Inkscape.
 - Les logos TCH entiers (texte + logo) ne contiennent plus les versions indépendantes afin d'éviter des soucis d'inclusion récursive dans Inkscape (autrement dit "sinon ça marche pas").
 - Les TVQ ont bien reçu leurs traductions en elfique/nain oubliées dans le commit précédent.

## 2021-06-26 (445cf56a) - Correction panneaux elfique/nain (commit de travail)
### Autres fichiers :
 - Les textes en elfique et en nain ont été réduits en taille dans les fichiers *info/signal_sonore_1x2.svg* et *acces_interdit/aip_1x2.svg*.
 - Le texte en nain a été corrigé dans *acces_interdit/aip_1x2* (problème de conjugaison).

## 2021-06-26 (a06e4860) - Langues elfique et naine (commit de travail)
### Autres fichiers :
 - Ajout du panneau *info/signal_sonore_1x2.svg* (ex-Serge le Lapin), et de l'icône associée *logo/fermeture_portes.svg*. (dp#220)

## 2021-06-25 (6e39f127) - Langues elfique et naine (commit de travail)
### Zones naturelles :
 - Ajout du Picbite, près de Grenat–Récif.
### Commerces :
 - Renommage de l'auberge du Presqu'Lac, qui s'appelle désormais le *Gîte Bachir*. 
### TCH :
 - Ajout d'une version en langue elfe et d'une version en langue naine de *metro_plan.svg* et *metro_plan_legende.svg*.
 - Simplification de *metro_plan.svg* et *metro_plan_legende.svg* pour retirer un tas de merdier ajouté par Inkscape et faciliter l'édition future.
 - Passage du style *metro_plan.css* dans un format compatible avec Inkscape pour faciliter son export (on **@import** le fichier css dans un style intégré au fichier au lieu d'utiliser le tag **xml-stylesheet**).
### Autres fichiers :
 - Ajout de polices pour les langues elfique (*font/TengwarAnnatar.ttf* et ses acolytes) et naine (*font/Erebor[1|2].ttf* et *Erebcap[1|2].ttf*). Le fichier de "licence" est inclu pour la police Cirth Erebor (*font/Erebor_readme.txt*) mais pas pour Annatar. J'ai bien essayé de la retrouver, mais le site original qui la distribuait a fermé depuis très longtemps et elle n'existe plus que sur des annuaires de polices où elle est disponible pour "usage non commercial".
 - Ajout du panneau *acces_interdit/electrocution_1x2.svg* et de l'icône associée *logo/electrocution.svg*.
 - Ajout des langues elfique et naine aux panneaux *acces_interdit/aip_1x2*, *1x2_g*, *1x2_d*, *2x1* et *3x1.svg*, ainsi qu'au panneau Vente rectangulaire (*info/vente.svg*).
 - Passage de plusieurs panneaux verticaux qui ne l'étaient pas au format carré, pour éviter de se faire chier ensuite à resize la texture pour l'import dans Minecraft.
 - Modification des styles de nombreux fichiers de TCH, car j'ai enfin compris quelle est la bonne manière d'utiliser les @font-face afin que cela fonctionne correctement sur Inkscape **et** ailleurs.

## 2021-06-08 (ad54bc08) - Panneaux corresp RER (commit de travail)
### Autres fichiers :
 - Les panneaux corresp (*corresp/corresp.svg*) du RER A ont été modifiés : ils n'affichent plus la direction précisément, mais simplement la direction (Nord ou Sud). (rp#69)
 - Les panneaux corresp du RER B ont été ajoutés selon la même logique (les directions sont Est et Ouest).
 - Certains logos TCH (*logo/m_n*, *rer*, *rer_b*, *rer_n*, *rer_transp*, *rera*, *rera_transp*, *rerb* et *rerb_transp.svg*) ont été nettoyés : des groupes vides ou inutiles ont été supprimés, et la taille indicative de chaque logo est passée à 256x256 pour éviter un souci lors de l'export d'images contenant ces logos sous Inkscape.

## 2021-06-07 (46d32e2f) - Logo TCH (commit de travail)
### Autres fichiers :
 - Modification des logos de TCH :
    + Le logo de TCH version rouge (*logo/tch_logo_r.svg*) n'a plus d'ombre. Il faudra l'ajouter manuellement si besoin est, de préférence à l'aide d'une fonction SVG (exemple dans les totems station).
    + Le texte version rouge (*logo/tch_texte_r.svg*) est également en dégradé, comme *tch_logo_r.svg*.
	+ Les versions combinées (*logo/tch_r*, *tch_b* et *tch_n.svg*) ne contiennent plus de chemins, mais simplement 2 images embed (*tch_X* contiendra *tch_texte_X* suivi de *tch_logo_X*).
 - Ajout de nouvelles versions transparentes de logos existants, pour les lignes de câble (*logo/ca_transp* et *cb_transp.svg*) et de RER (*logo/rera_transp* et *rerb_transp.svg*).

## 2021-06-07 (b82bad89) - Réservé carte Gold (commit de travail)
### Autres fichiers :
 - Ajout du fichier *info/reserve_gold.svg*, fait pour être placé devant les valideurs Gold. (rp#34, dp#220)

## 2021-06-06 (0c99d43b) - Accès quais & corresp multiples (commit de travail)
### Autres fichiers :
 - Les fichiers *acces_quais/2x1.svg* et *3x1.svg* contiennent désormais également les accès aux quais multiples. (rp#86)
 - Le fichier *corresp/corresp.svg* contient désormais également les corresp multiples. (rp#101) **Les corresp multi-modes (par exemple RER A + M1) ne sont pas encore incluses, en attendant de statuer sur le design approprié.**
 - Il est possible d'aller au tout début ou à la toute fin du tableau des panneaux, dans *acces_quais/* et *corresp/*, en utilisant les touches PageUp/PageDown.
 - Le maximum horizontal est désormais overridé pour les *corresp/* dans *corresp.svg*, puisque les derniers éléments ne sont pas les plus à droite.

## 2021-06-05 (598635d8) - TVQ 3 (commit de travail)
### Autres fichiers :
 - Correction des flèches TVQ de l'accès aux quais 3x1 qui étaient mal nommées.

## 2021-06-05 (ee39a5ef) - TVQ 2 (commit de travail)
### Autres fichiers :
 - Correction des flèches R et TVQ de l'accès aux quais 3x1 qui étaient mal positionnées dans leur transform.

## 2021-06-05 (bab64018) - TVQ (commit de travail)
### Autres fichiers :
 - Ajout des panneaux TVQ (trafic via quai) aux *acces_quais/2x1.svg* et *3x1.svg*, ainsi que de flèches "invisible avec bande bleue" (**fleche_r_g/d**) et "invisible sans bande bleue" (**fleche_tvq_g/d**). (rp#34, dp#220)
 - Ajout de l'icône *logo/quai.svg*, qui est utilisée par les panneaux TVQ.
 - Modification du script *acces_quais/acces_quais.js*, qui calcule désormais une variable "yMax" au lieu d'assumer qu'il s'agira tjrs de 0.

## 2021-06-05 (92bdc9b8) - Totems de station 2 (commit de travail)
### Autres fichiers :
 - Réparation du fichier *totem_station/totems_station.svg* : les hauteurs des images incluses étaient implicites au lieu d'être explicites, et n'étaient donc pas correctement dimensionnées par Inkscape.

## 2021-06-05 (487d89d0) - Totems de station (commit de travail)
### Autres fichiers :
 - Ajout dans *logo/* de nouvelles versions des logos :
    + **métro** (*m_b* blanc fond transparent, *m_n* noir fond transparent)
	+ **rer** (*rer_b* et *rer_n* comme pour métro)
	+ **cable** (*cable_b* et *cable_n* de la même manière, + *cable_transp* qui n'existait pas encore)
 - Ajout dans *totem_station/* de totems de station pour les différentes combinaisons de modes de transport (métro/câble/RER), au format 1x1 (256x256px). (rp#34, dp#220)

## 2021-06-04 (068a00f0) - Ajout commerces (commit de travail)
### Commerces :
 - Ajout de l'auberge de l'Huis du Rusé (Tolbrok).
### Git :
 - Passage du changelog au format Markdown, et renommage en **CHANGELOG.md**.

## 2021-04-30 (f84adb4e) - rp#77 (commit de travail)
### Autres fichiers :
 - La sortie 3 de Procyon a été ajoutée aux SVG de *sortie/*. (rp#77)

## 2021-04-27 (4881584f) - Bugfix Accès interdit (commit de travail)
### Icônes :
 - Conversion du transform du groupe du personnage (dans *logo/interdit_au_public.svg*) pour corriger un mauvais positionnement lors de l'export dans Inkscape.

## 2021-04-27 (5980c7a3) - Accès interdit 2 (commit de travail)
### Icônes :
 - Optimisation de l'icône "sens interdit" qui contenait des lignes inutiles.
 - Ajout d'une icône "interdit au public" (personnage cerclé et barré de rouge).
### Autres fichiers :
 - Ajout du panneau "Accès interdit au public" (utilisé notamment en bord de quai) dans *acces_interdit/aip_1x2.svg*.
 - Suppression des références à des scripts incluses par erreur dans les fichiers "accès interdit" du commit fbbfbab4.

## 2021-04-27 (fbbfbab4) - Accès interdit (commit de travail)
### Icônes :
 - Une icône de sens interdit a été ajoutée (dans icon/sens_interdit.svg*). Elle inclut une légère ombre en bas à gauche par défaut.
### Autres fichiers :
 - Les panneaux accès interdit existent (dans *acces_interdit/*) dans toutes les tailles actuellement incluses IG : 2x1 et 3x1 pour les horizontaux, et 1x2_g/d pour les verticaux.

## 2021-04-27 (a7f06fcd) - Version 2.12
### Fond de carte :
 - Mise à jour du fond de carte (septembre 73) pour la moitié sud de la carte (Z = 3000 et plus). (13f54624)
 - Le fond noir a été retiré du groupe fond de carte, pour qu'il reste affiché en toutes circonstances y compris si on désactive le fond de carte. (49333f99)
 
### Affichage :
 - Déplacement de l'intégralité des chemins, cercles, rectangles et images considérés comme des objets de la carte (zones naturelles, villes, lieux-dits, rivières, chemins...) pour les aligner avec les positions ingame (le point 0,0 du monde, au Hameau, est désormais le point 0,0 de la carte). Il reste de NOMBREUSES zones non corrigées qui ont actuellement des formes incorrectes. (31063dee)
 - L'interface de la carte a été totalement modifiée ; on déplace désormais "la carte", qui est un élément SVG inclus dans le SVG principal, ce qui permet à l'interface principale d'être statique et à l'avant-plan de la carte. De nombreux menus historiquement disponibles ne sont pas encore accessibles (recherche, aide...). (31063dee) (#6)
 - De nombreuses miniatures ont été ajoutées pour affichage dans la barre d'information lors de la sélection d'un lieu (ville ou station de métro principalement pour l'instant, avec également quelques lieux-dits). (31063dee)
 - Les icônes de commerces ajoutées dans la version précédente ont été déplacées vers un nouveau calque principal, calqueCommerces. (31063dee)
 - Le déplacement de la carte fonctionne à nouveau correctement sur Chrome. La fonction de déplacement dans le JS a été réécrite et simplifiée, et utilise désormais le delta de la souris plutôt que de calculer le déplacement à partir de sa position. (#17) (3f9e13f2)
 - Les miniatures d'illustration des villes/lieux-dits ne restent plus affichées après avoir été désélectionnées. (3f9e13f2)
 - Le splash ne devrait plus disparaître avant que les cartes ne soient toutes chargées. L'animation de l'écran de démarrage se joue également correctement sur Chrome. (#16) (3f9e13f2)
 - Le splash se masque plus tôt après le chargement pour éviter que l'on ne puisse pas scroller juste après sa disparition.
 - Les éléments activables/désactivables sont détectés automatiquement au chargement de map.svg et ajoutés à la légende avec des checkboxes permettant de les toggle facilement. (3f9e13f2)
 - Ajout de 2 icônes de checkboxes, en version on et off, pour la sélection des calques (dans *icon/checkbox*_on et _off). (3f9e13f2)
 - L'écran de chargement, à l'étape du chargement des cartes, affiche désormais un indicateur de progression. (3f9e13f2)
 - *map.svg* se scale désormais automatiquement à la taille de la fenêtre qui l'affiche. (3f9e13f2)
 - Ajout d'une icône à gauche du nom de l'élément sélectionné, afin de centrer la vue sur cet objet (similaire à un double clic sur l'objet). (49333f99)
 - Réparation d'un souci dans le JS qui finissait par faire disparaître le texte lorsqu'on avait beaucoup redimensionné le cadre. (49333f99)
 - Correction d'un souci dans la transformation de l'interface en fonction de la taille de l'écran, qui faisait que les infobulles des boutons n'étaient pas visibles. (49333f99)
 - Une infobulle s'affiche désormais sur la gauche si on est trop proche de la droite de l'écran. (49333f99)
 - La recherche est à nouveau fonctionnelle sous la forme d'un menu à déplier comme la légende, en bas à droite de l'écran. (49333f99)
 - Création d'une icône de loupe pour symboliser l'interface de recherche lorsqu'elle est minimisée. (49333f99)
 - La police par défaut de la carte est désormais une sans-serif. (49333f99)
 - L'affichage des coordonnées affiche à nouveau l'ID de carte correspondant. (#14) (49333f99)
 - L'affichage de la grille est à nouveau aligné avec les cartes ingame. (49333f99)
 - Les limites du zoom ont été agrandies en X pour éviter sur Firefox un effet de tressautement de l'image très désagréable. (49333f99)
 - Ajout d'une icône pour les sommets de montagne. (*icon/pic.svg*). (13f54624)
 - Ajout d'un message indiquant que Javascript doit être activé, sur l'écran de chargement de *map.svg*. (13f54624)
 - La sélection d'un projet ne devrait plus faire disparaître le calque des projets si les régions sont masquées, ni la sélection d'un lieu-dit les masquer si les routes sont masquées. (#19) (13f54624)
### TCH :
 - Les lignes de transports en commun ont désormais également un chemin "principal" invisible, plus large que les tracés individuels, pour permettre une sélection de leurs tracés avec 99% d'arrachage de cheveux en moins par rapport à un tracé de câble ordinaire. (désactivé dans certains noeuds comme Le Hameau pour éviter de bloquer certaines lignes à la sélection) (13f54624)
 - Traçage de la partie nouvellement construite de la ligne M8b, et repositionnement des stations Moldor–Balchaïs et Relais. (13f54624)
 - Retraçage du câble A, déplacement de la station "Évrocq-Le Haut" et ajout de la station "Évrocq-Le Bas". (13f54624)
 - Renommage de "Gare d'Évrocq" en "Évrocq–Sauvebonne". (13f54624)
 - La station Hameau RG est bien agrandie lorsqu'on sélectionne HameauRD, et vice versa. (#10) (49333f99)
 - La station Crestali a été repositionnée, car elle était mal alignée avec le tracé du métro 8. (49333f99)
 - La station de métro de Grenat affiche bien un point de la couleur du M1 au lieu de celle du M2. (31063dee)
 - Les performances lors de l'affichage du calque métro ont été optimisées. (31063dee)
### Zones naturelles :
 - Les tracés de l'île de Grenat, des monts Albrok, du massif Gorvedin et des montagnes de Krypt ont été simplifiés. (31063dee)
 - Retraçage de la mer d'Yrin, du désert des Sablons, des dunes de Kalig, de la forêt noire de Cal'vih, du désert Laar et de la savane de Zobun qui avaient été abîmés par la conversion aux nouvelles coordonnées. (3f9e13f2)
 - Réparation du tracé de la forêt de Thoroïr. (3f9e13f2)
 - Retraçage de la forêt d'Évenis pour simplifier le tracé. (3f9e13f2)
 - Changement des ID des zones ayant reçu un nom définitif, afin qu'une recherche sur "XX" ne les fasse plus apparaître. (49333f99)
 - Simplification du tracé des plaines Iocus et Briomir afin de moins se casser la tête et d'avoir une seule zone au lieu de deux. (49333f99)
 - Rajout de la presqu'île du Poulet, pour nommer une autre presqu'île que l'initiale (qui a été détruite). (13f54624)
 - Suppression de diverses zones naturelles temporaires (XX), qui n'existent plus. (13f54624)
 - Fusion de XXMaraisC à XXMaraisB en raison d'une terraformation ambitieuse. (13f54624)
 - Retraçage du canal de la Flumine et du XXMaraisD à partir des cartes nouvellement updatées. (13f54624)
 - Suppression des cours d'eau XXFleuveRelaisMerEst, XXFleuveQuecolMerEst, XXRiviereMoldorMerEst, XXRiviereDrieMoldor qui n'ont finalement pas été construits de cette façon IG. (13f54624)
 - Ajout des cours d'eau définitifs de la région du nord des Balchaïes, jusqu'à la Flumine. (13f54624)
 - Retraçage des Balchaïes, un peu réduits au nord pour laisser la place à un nouveau massif. (13f54624)
 - Agrandissement du massif des Fourgus pour coller à la nouvelle terraformation de la région. (13f54624)
 - Ajout du XXMaraisJ (au bout du XXFleuveQuecolEst2). (13f54624)
 - Conversion des pics existants (sous forme de zones semi-transparentes) au nouveau format (icône *pic.svg* + texte avec l'altitude). Ajout dans la foulée de nombreux pics mineurs (essentiellement à Krypt et dans les Balchaïes) avec des noms temporaires. (13f54624)
### Villes et lieux-dits :
 - Ajout des commerces nouvellement construits à Quécol-en-Drie (taverne, auberge, tabagie, marché).
 - Retraçage de Quécol sous forme de 2 zones disjointes (Quécol-en-Drie et Quécol-le-Bas). (13f54624)
### Chemins :
 - Suppression de la route 190, et retraçage des routes 180 et 193, en raison du remodelage intensif du terrain. (13f54624)
### Projets :
 - Suppression du projet de création du canal de la Flumine, qui existe désormais IG. (13f54624)
### Régions :
 - Lorsqu'on sélectionne une région sous le joug d'une autre, cette dernière n'est plus highlightée automatiquement. L'information reste affichée dans la barre en bas de l'écran. Toutes les régions possédées sont bien mises en valeur lorsqu'on sélectionne la région capitale. (3f9e13f2)
### Autres fichiers :
 - Les panneaux nom station ont désormais un fond du même bleu que les panneaux sortie. (rp#74) (9c8b55da)
 - Différentes couches de la skybox ont été modifiées, les SVG édités sont ceux utilisés dans la dernière version du RP. (854e2c8f)
### *Détail des commits*
 + [Commit de travail - Màj de la carte septembre 73] (13f54624)	- 2021-04-27
 + [Commit de travail - rp#74]	(9c8b55da)		- 2021-03-12
 + [Commit de travail - Edit du ciel]	(854e2c8f)		- 2021-03-12
 + [Commit de travail 3 - issue #6]	(49333f99)		- 2021-02-24
 + [Commit de travail 2 - issue #6]	(3f9e13f2)		- 2021-02-24
 + [Commit de travail - issue #6]	(31063dee)		- 2021-02-23
---


## 2021-02-22 (cf7ce0d8) - Version 2.11
### Villes et lieux-dits :
 - Modification du tracé de Fort du Val pour coller à la forteresse nouvellement construite, qui comprend 2 nouvelles zones liées : son port et son village.
 - Déplacement de la Dodène pour coller à sa nouvelle position IG.
 - Agrandissement d'Ygriak pour coller à la nouvelle partie construite IG.
 - Création du village d'Asvaard et du village de pêcheurs au nord-ouest d'Arithmatie.
 - Simplification des chemins du Hameau et de Grenat.
### Zones naturelles :
 - Ajout du Glacier des Pics, du fleuve de Fort du Val.
 - La Baie de Procyon a été renommée Baie de Ratec (#15).
### Villes et lieux-dits :
 - Ajout de la plupart des tavernes, auberges, etc... de la carte, sous la forme d'icônes plutôt que de zones sélectionnables puisqu'il s'agit de lieux restreints.
### Projets :
 - Ajout d'un projet de réfection de la mer d'Étherium. (#12)
 - Ajout d'un projet de déplacement du CGC dans les falaises de Knosos. (#13)
 - Ajout d'un projet de construction d'un observatoire astronomique dans le désert Laar.
### TCH :
 - Modification du tracé du câble B et des positions de ses stations (*map.svg*) pour coller à ce qui a été construit et est désormais visible sur la carte, et légère modification du tracé du métro 8 pour s'adapter à ces modifications.
### Affichage :
 - Ajout d'icônes représentant des édifices importants sur la carte, dans *icon/* : *boisson.svg* pour les tavernes, *nourriture.svg* pour les restaurants et les auberges, *chambre.svg* pour les auberges et les hôtels, *tabac.svg* pour les tabagies, *livre.svg* pour les bibliothèques et librairies, *magasin.svg* pour les magasins généraux et divers.
### Fond de carte :
 - Mise à jour des cartes d'une partie du territoire (entre les latitudes 0-2048, 5120-7168). (mi-avril 72)

## 2021-02-20 (97bfca53) - Version 2.10f
### TCH :
 - Ajout des logos ascenseur et escalier en versions bleu sur fond blanc et blanc sur fond bleu. L'escalier existe en 4 versions, selon qu'il est montant ou descendant et si le personnage va vers la gauche ou la droite.
 - Ajout des panneaux sortie 2x1 et 3x1, à la fois neutres/numérotés (versions petites et grandes) et détaillés par station et sortie (versions petites uniquement). Les versions petites font la même taille que les accès aux quais (96px), tandis que les grandes mesurent 160px ; ceci afin d'occuper exactement 1 bloc si l'on empile 1 sortie neutre + 1 accès aux quais
 - Ajout des PID terminus aux fichiers des PID (*pid/2x1.svg* et *3x1.svg*), car on avait oublié.
 - Modification du code des correspondances (*corresp/corresp.svg* et *corresp.js*) pour éviter quelques soucis à l'exportation sur Inkscape et simplifier le clipping en n'utilisant qu'un chemin de clip au lieu des 6 existants.

## 2021-02-13 (7cc13631) - Version 2.10e
### TCH :
 - Les directions des panneaux Correspondance sont désormais indiquées en Parisine Bold au lieu de Parisine simple.

## 2021-02-13 (2bc2273d) - Version 2.10d
### TCH :
 - Ajout des fichiers permettant de générer des panneaux "Correspondance" 1x2. (*corresp*)
 - Ajout des logos des métros 9 à 14 (dans *logo*, ainsi que dans le fichier *logo/_export.svg*).
 - Ajout du fichier permettant de générer les "lettres" des lignes de transports IG. (*logo/alphabet.svg*).
 - Ajout de la station "Asvaard–Rudelieu" aux *nom_station* 2x1 et 3x1, ainsi qu'au plan du métro. (rp#38)

## 2021-02-06 (f56ee6ab) - Version 2.10c
### TCH :
 - Ajout des fichiers permettant de générer des panneaux d'accès aux quais (*acces_quais*), des PID (*pid*), des panneaux vente (*info/vente* et *vente_1x1*).
 - Ajout des fichiers permettant de générer les noms de station à la taille RER (4x2).
 - Ajout des fichiers permettant de générer les différentes couches de la skybox (dans *sky*).
 - Les logos du métro et du RER (*/logo/m* et *rer*) existent désormais en deux versions, avec fond blanc ou sans fond (transparent).
 - Les logos de TCH existent désormais en une version supplémentaire, de couleur rouge (logo original vectorisé). Ils se trouvent dans *logo/tch_logo_r* et *tch_texte_r*.

## 2020-11-08 (427d5d1f) - Version 2.10b
### Affichage :
 - Remplacement des filtres de floutage par des dégradés précalculés sur les calques de géographie pour l'amélioration des performances globales.

## 2020-11-08 (966e08df) - Version 2.10
### Villes et lieux-dits :
 - Inversion du nommage des villages AG et AI pour coller à la réalité IG.
### Zones naturelles :
 - Ajout de la rivière reliant le lac de Miopatha au lac Ukufa.
 - Retraçage simplifié de la mer de Grenat, et découpage de différentes baies et criques de manière plus large qu'auparavant (notamment la Baie de Procyon et celle d'Oréa-sur-Mer).
 - Retraçage de la crique de Sanör, du lac d'Ondine, des lacs Morador, Opale, Vrarr et Duerilïn (pour les 4 derniers, j'espère qu'on terraformera la forêt d'Evenis prochainement, pour pouvoir virer ces flaques de merde).
 - Retraçage des lacs de Cerileth, de Saranel, de la rade de Niascar, de la crique d'Embruine, du lac Ukufa, de la Côte Maudite.
 - Rétrécissement de la mer de Grenat pour ne pas offenser les Procyens.
 - Suppression du Bras Cristallin, qui va bien DÉGAGER ingame dans le futur.
 - Suppression du lac Kersë, qui a été supprimé ingame il y a un petit moment déjà.
 - Suppression de la mare de Norir, dont il est assez improbable qu'elle ait conservé un nom sachant que c'est une simple FLAQUE parmi des milliers d'autres flaques, en plus du fait qu'elle se trouve dans la savane d'Alwin et va donc bien se barrer lorsqu'on aura terraformé la savane en désert.
### Projets :
 - Ajout d'un projet d'extension du désert des Sablons côté Illysia/Chizân.
 - Ajout de projets pour le déplacement de différentes îles de l'archipel de Grenat, pour le creusage de la Flumine (canal entre la mer de Grenat et l'océan de l'est).
 - Ajout de 3 projets de suppression de pics glacés dans l'Est de la Haute Mer de Grenat.
 - Ajout d'un projet de création du Récif de Grenat.
 - Suppression des projets liés au lac de Zobun (incorrectement nomenclaturés Alwin) en raison d'un changement d'avis.
 - Remplacement du projet "Suppression du Sud du Massif Gorvedin" par "Extension Nord du Massif Gorvedin" en raison du déplacement de l'Île du Récif.
### Affichage :
 - Accélération du fondu de fin de l'écran de chargement.
 - Ajout de la gestion des lieux relatifs pour les champs ne le supportant pas encore (comme les projets).

## 2020-11-08 (9b7c0111) - Version 2.09f
### Zones naturelles :
 - Retraçage de la cordillière d'Yper et ajout de 3 massifs (W, X, Y).
 - Retraçage de 2 rivières non nommées dans la même région.
### Villes : 
 - Ajout de 3 nouveaux villages (AG, AH, AI).
### Projets :
 - Ajout de 3 projets en lien avec les nouvelles zones découvertes (Grand Ratiboisage, extension massifs Yper/Eidin et W).
### Fond de carte :
 - Ajout des cartes HD de la partie nord-est qui était en SD. (août 67)
 - Suppression des cartes SD temporaires.

## 2020-11-07 (d55a2287) - Version 2.09e
### Fond de carte :
 - Ajout des cartes SD de la partie au nord-est qui était manquante. (août 67)

## 2020-11-07 (b622b103) - Version 2.09d
### Affichage :
 - La position sélectionnée par le pin est désormais plus précise.
 - La position n'est plus réinitialisée lorsqu'on masque le calque des coordonnées.

## 2020-11-07 (a7987b4e) - Version 2.09c
### Affichage :
 - L'affichage des coordonnées a été restauré. Il est comme toujours activable/désactivable avec la touche X. (#8)
 - Ajout pour ces fins de 2 icônes de punaises (la seule utilisée pour l'instant est *icon/pin_from.svg*, mais *pin_to.svg* servira pour les mesures de distance et d'itinéraire quand celles-ci seront implémentées).

## 2020-11-07 (e4cbffa2) - Version 2.09b
### TCH :
 - La ligne 5b a été entièrement tracée conformément à sa position ingame (date de la version précédente, mais non indiqué).
 - La ligne 6 a été entièrement tracée également, et est bien indiquée comme ouverte. (#5)
 - Les stations des lignes 5b, 6 et 7bis qui étaient incorrectement positionnées ont été rectifiées. (et Dorlinor est positionnée de manière cohérente, #2)
 - La station Frambourg–Ternelieu a été remise à l'endroit. (#9)
 - Illysia a été ajoutée sur *metro_plan.svg*. (#11)
### Affichage :
 - Il y a désormais un fondu lorsqu'on affiche ou masque un calque.
 - TCH est désormais un calque principal, et les calques RERLignes, MétroLignes, stations, etc... sont ses enfants.
 - Géographie est désormais un calque principal, et les calques de géographie sont ses enfants.
 - Le texte d'info "Appuyez sur H pour afficher l'aide" s'affiche à nouveau correctement.
 - Nouvelle optimisation des performances en désactivant l'effet de hover des calques de géographie. Il faut désormais cliquer dessus pour afficher la surbrillance, mais le déplacement et le dézoom sont nettement plus fluides.
 - Le calque de géographie est par conséquent activé par défaut.
 - Le calque des chemins a été placé au-dessus de celui des cours d'eau, qui passe également derrière le calque des lieux-dits.
 - L'animation de fondu ne devrait plus bugger lorsqu'on appuie rapidement plusieurs fois sur la même touche.

## 2020-11-06 (93c87e36) - Version 2.09
### Zones naturelles :
 - Suppression de lieux disparus suite à terraformation (Lagon de Merlongue, Crique d'Eaucourbe, Presqu'Île du Poulet, Estuaire de Salport)
 - Passage de plusieurs autres cours d'eau en nom raccourci (Fleuve d'Okh'Okhu => Okh'Okhu, etc).
### Villes et lieux-dits :
 - Retraçage du château Onyx en raison d'un bug d'affichage.
### Régions :
 - Retraçage des régions et modification de plusieurs noms pour être davantage RP.
 - Certaines régions ont été associées ensemble, et cliquer sur l'une d'entre elles affiche également les autres avec une légère surbrillance (ex: Grenat + Territoires revendiqués par Grenat, ou Hameau + Fort du Val).
### Projets :
 - Suppression de projets en raison de leur réalisation ingame (Création du Val de Fort du Val, Extension Sud-Ouest des Montagnes de Krypt, Suppression de l'Ouest du Fleuve du Crochu, Extension Est des Montagnes de Krypt, Extension Ouest de la Mer d'Etherium, Extension Sud de la Mer d'Etherium, Raccordement du Fleuve du Crochu au Lac Ukufa).
### TCH :
 - Les lignes 1 et 2 ont été correctement inversées sur le fichier map.svg. (#7)
 - Les lignes et les stations dans lesquelles elles passent sont désormais mutuellement considérées comme relatives les unes aux autres, et s'affichent en surbrillance quand l'une d'elles est sélectionnée.
### Affichage :
 - Le calque des cours d'eau s'affiche désormais au-dessus du calque des villes et de celui des lieux-dits.
 - Optimisation importante des performances lorsque les calques de géographie sont affichés.
 - Optimisation de la taille des hachures des projets.
 - L'animation de sélection des fleuves a été modifiée, et elle devrait également se jouer à l'envers lors d'une désélection.
 - Une animation de sélection a été ajoutée aux stations de métro et gares RER pour améliorer leur agréabilité visuelle.
 - Une animation de sélection a été appliquée aux lignes elles-mêmes.
 - Une animation de sélection a été appliquée aux régions, aux villes et aux lieux-dits, ainsi qu'aux chemins.
 - Nettoyage de fond en comble des sections devenues inutiles de *map.css*. Quelques améliorations aussi dans *map.js*, mais il reste du travail à faire pour supprimer ce qui n'est plus utile et compacter ce qui l'est encore.
 - Suppression de quelques champs devenus inutiles de *map.svg*.
 - Les zones géographiques ne devraient plus s'auto-afficher lorsqu'on recherche et sélectionne un fleuve ou une rivière.
 - Un écran de chargement a été ajouté pour ne pas assister au carnage visuel initial.

## 2020-11-05 (e8dedfe5) - Version 2.08
### Zones naturelles :
 - Retraçage de tous les fleuves pour prendre en compte leur nouvelle méthode de rendu (en tant que chemins non remplis et animés, pour les rendre à la fois plus visibles et plus légers).
 - Ajout de très nombreux fleuves et rivières, certains ayant déjà le tracé indiqué et d'autres devant être terraformés.
 - Renommage de nombreuses rivières et fleuves pour enlever la redondance de "Rivière de XX" ou "Fleuve de XY" (par exemple, Rivière de Trismagus => Trismagus, Fleuve du Crochu => Le Crochu).
 - Retraçage de la forêt de Sulûm.
 - Suppression du torrent d'Omosh, de la Drurisée, des rivières Jëej et de Rizzen, des fleuves Rieubois, Marembrum et Rondel en raison de leur disparition.
 - Suppression pour les mêmes raisons du bois de Sulûm, des XXForetBL, XXForetBK2 et BK3, XXForetBN, XXForetBI et BI2
### Projets :
 - Remplacement du tracé du projet "Extension Est du Massif H" : le tracé précédent a été réalisé, mais une nouvelle extension sera nécessaire.
### Fond de carte :
 - Mise à jour des cartes de la moitié nord du territoire (de Chizân à Moronia). (5-9 juillet 67)

## 2020-10-27 (f945e6c6) - Version 2.07
### Villes et lieux-dits :
 - Retraçage d'Aulnoy, d'Argençon, de Crestali, de Bussy-Nigel, de Calmeflot, de Milloreau, de Géorlie, de Frambourg et de Ternelieu.
### Chemins :
 - Ajout des routes 201 (Procyon—Aulnoy), 205 (Aulnoy–Argençon).
### Zones naturelles :
 - Ajout du XXCanyonA qui vient d'être terraformé dans le désert des Sablons.
 - Ajout des lacs XXLacArgencon et XXLacAulnoy, ainsi que du XXFleuveAulnoy et de la XXRiviereArgencon.
 - Retraçage du Désert des Sablons.
 - Retraçage de l'Étang Vaskar et renommage en Lac Vaskar.
 - Retraçage des Dunes de Kalig et du Canyon de Kalig, qui ont été agrandis.
 - Retraçage des fleuve Brimiel, Elkantar et Nambêk.
 - Suppression des Collines d'Erêmos et de la presqu'île de Thekñ en raison de leur disparition IG.
 - Suppression du fleuve Narsus, de la rivière de Lothion, de l'île FFIle01 en raison de leur disparition IG.
 - Suppression de la rivière Iswol et du lac Isowo en raison de leur disparition IG.
 - Suppression de la rivière Atodem en raison de sa future suppression IG.
### Projets :
 - Ajout d'un projet de suppression de la savane d'Alwin, qui deviendrait une zone littorale située au sud du nouveau désert Onyx, au bord de la mer d'Etherium.
 - Suppression des projets "Désertification des Collines d'Erêmos", "Création de canyons dans le désert des Sablons", "Suppression de la rivière Lothion", "Suppression du fleuve Narsus", "Extension Sud-Ouest de la savane d'Alwin", "Suppression du nord du fleuve Brimiel", "Raccordement du fleuve Brimiel à la rivière Atodem" en raison de leur réalisation IG.
### Fond de carte :
 - Mise à jour des cartes de l'intégralité du territoire exploré. (16-22 janvier 67).
### Divers :
 - Nettoyage de dégradés inutilisés dans le fichier *map.svg*.

## 2020-10-24 (0e75742b) - Version 2.06c
### TCH :
 - Ajout d'un fichier exporteur pour les logos du métro (*logo/_export.svg*) ; chaque logo est sur un calque séparé.
 - Modification des dimensions des logos métro (*logo/1* à *8b.svg*), qui occupent désormais l'intégralité des 256x256px.
 - Réparation d'un souci dans le code de génération des totems, qui ne remplaçait pas correctement les ç par des c dans l'ID.

## 2020-10-24 (615479e5) - Version 2.06b
### TCH :
 - Mise à jour de la couleur de la ligne 6 (à l'origine #007852 comme la 12, elle devient #60bb39).

## 2020-10-24 (e5741a02) - Version 2.06
### TCH :
 - Ajout des panneaux nom_station (*nom_station/nom_station.svg*).
 - Ajout de la légende du plan du réseau (*metro_plan_legende.svg*).
 - Modification de la manière dont la police personnalisée est chargée dans les SVG, pour permettre de la charger directement depuis les polices système lorsqu'elle est déjà installée.
 - Modification de la station-type "Correspondance" (*station/x.svg*), dont la VB était rectangulaire au lieu d'être carrée.
 - Fusion des totems (*/totem/<id_ligne>_<id_station>.svg*) dans un seul fichier */totem/totems.svg*. Il est manipulable via Javascript depuis un navigateur Internet : utiliser Gauche/Droite pour changer de ligne, Haut/Bas pour changer de station, et Ctrl+E pour séparer chaque totem sur des calques individuels (utiliser ensuite Ctrl+S pour enregistrer le fichier SOUS UN AUTRE NOM afin d'exporter les .png pour le resource pack).

## 2020-10-20 (296f56b7) - Version 2.05
### TCH :
 - Ajout des totems de toutes les lignes de métro et de câble (dans */totem*).
 - Ajout de nouvelles stations pour chaque ligne avec une flèche descendante, pour utilisation dans les totems.
 - Nettoyage du fichier *metro_plan.css* des règles copiées/collées de *map.css* et qui n'étaient plus utiles.
 - Modification des couleurs des RER A et B sur les plans et les logos.
 - Modification de la couleur du bleu des bordures de panneaux.

## 2020-10-20 (929b9f6a) - Version 2.04
### TCH :
 - Ajout du plan métro simplifié, ainsi que de nouvelles icônes de stations et de logos pour les besoins dudit plan.
 - Modification des couleurs des lignes pour coller avec la nouvelle charte.
 - Nommage de la station Moldor–Balchaïs.
### Zones naturelles :
 - Nommage du val de Moldor.

## 2020-07-04 (0268af81) - Version 2.03b
### Mise à jour des cartes le long de la ligne 6.

## 2020-06-17 (bd7588f5) - Version 2.03
### Projets :
- Ajout du projet "Réchauffement de la forêt d'Odhur".
- Ajout du projet "Extension de la savane d'Alwin", omis en 2.02 (remplacement de la steppe de Palion).
- Ajout de divers projets (Création de canyons dans le désert des Sablons, extension Est des montagnes de Krypt, extension Ouest de la mer d'Etherium, création d'un lac dans la savane d'Alwin + 2 raccordements relatifs à ce lac, suppression de l'aval de la rivière de la Tonnelle, déplacement d'Athès, assèchement de Brâahm, désertification d'Erêmos).
- Extension du projet "Extension Nord de la forêt de Väam" à l'ensemble du désert Dralarien et une partie de la plaine de Bérielle.
- Achèvement du projet "Suppression de la rivière Lypolle".
- Achèvement du projet "Suppression du sud de la rivière Brerain".
- Suppression du projet "Extension et densification de la forêt noire de Cal'Vih" ; malgré l'absence de finition, la zone prévue a bien vu ses biomes modifiés et des arbres lui être ajoutés.
- Achèvement des projets "Extension nord-est du désert Onyx" et "Extension sud-ouest du désert Laar" (les zones prévues ont été légèrement rognées pour s'adapter au relief existant).
- Achèvement du projet "Extension est du désert de Deliah".
- Achèvement du projet "Extension sud-ouest du marais de Malcheiroso".
- Achèvement du projet "Raccordement de la rivière de Brerain au marais de Ratoderr".
- Achèvement du projet "Extension Nord-Est de la savane de Zobun".
- Achèvement des projets "Extension Nord-Est de la mer d'Etherium" et "Extension Nord-Est des falaises de Knosos".
### Zones naturelles :
- Retraçage de diverses zones dans la région de Dorlinor (Forêt noire de Cal'Vih, Désert Laar, Savane de Zobun, Désert Onyx, Marais de Ratoderr, Marais de Malcheiroso, Savane d'Artherr).
- Retraçage de la mer d'Etherium (en incluant le tracé prévisionnel du projet "Extension Sud de la mer d'Etherium").
- Simplification du tracé de la mer de Grenat (diminution de la précision).
- Suppression de la rivière Lypolle, qui n'existe plus à l'état naturel.
- Suppression du bosquet des Mille Chênes, qui a été plus ou moins avalé par la forêt de Cal'Vih.
- Suppression de la Dune d'Illios, qui n'est plus vraiment une dune.
- Suppression de la Colline du Polype, dont les dimensions ridicules ne justifient pas qu'elle ait un détourage et un nom spécifiques – d'autant plus qu'elle est désormais en plein milieu de la forêt noire de Cal'Vih.
- Suppression de la plaine Forandas, recouverte par la forêt noire de Cal'Vih.
- Suppression de la vallée de Laastek, par crainte que cela ne renvoie plus à grand chose ingame.
- Suppression de l'Iswol, qui est désormais une partie de la mer d'Etherium.
### Mise à jour des cartes de la moitié nord du territoire (Quatre Chemins & tout ce qui se situe plus au nord). Date des nouvelles cartes : 9-10 juin 64.

## 2020-05-16 (f8bf07df) - Version 2.02b
### TCH :
 - Modification de la couleur du câble B.
 - Suppression de la gare d'Alwin–Sablons.
 - Requalification de la station Évenis–Éclesta en terminus simple (il était resté par erreur avec l'icône Correspondance).
 - Suppression de la gare de Grenat–Arithmatie, qui devient une station de la ligne 5b uniquement.

## 2020-05-16 (cae9d805) - Version 2.02
### Projets :
 - Ajout du calque des projets de construction/terraformation. Il est accessible via la touche P.
 - Ajout de 33 projets de terraformation dont diverses suppressions de rivières, fusions de chaînes de montagnes et agrandissements de déserts ; ils sont presque tous listés ci-dessous dans Zones naturelles.
### Zones naturelles :
 - Ajout du Mont Dieu, du Mont Lacté, de la Baie de Procyon.
 - Agrandissement des montagnes de Krypt, fusionnées avec le XXMassifT.
 - Agrandissement des montagnes d'Éclesta, fusionnées avec le XXMassifK et le XXMassifL. L'opération supprime la plaine de Théade et le XXMaraisF.
 - Agrandissement du massif Cyclopis, qui supprime les XXForets W et W2, et la XXPlaineI.
 - Agrandissement du désert Laar, qui remplace une partie du désert de Deliah.
 - Agrandissement du désert de Deliah, qui se rapproche légèrement de Midès, ce qui diminue la taille de la XXPlaineY.
 - Agrandissement de la savane d'Alwin, fusion avec les collines d'Alwin, et suppression des zones vouées à disparaître que sont la forêt de Föof, la steppe de Palion et la prairie d'Elin.
 - Agrandissement de la forêt noire de Cal'Vih, fusion avec la forêt de Cemaën.
 - Agrandissement de la foret de Liobrus, fusion avec XXForetLiobrus2.
 - Agrandissement de la forêt de Väam.
 - Rétrécissement de la forêt de Jëej et suppression de la plaine de Jëej, pour coller avec la terraformation future.
 - Rétrécissement du désert Onyx au sud, au niveau de l'agrandissement de la mer d'Etherium.
 - Agrandissement du désert Onyx au nord-est, qui remplace une bonne partie de la forêt de Brimborion.
 - Agrandissement du désert des Sablons (au nord, pour le raccorder à la mer Heraën), qui remplace la plaine de Merrinid.
 - Agrandissement de la savane d'Artherr au sud, qui remplace une partie de la prairie de Flipp, ainsi que le lac d'Ehraad.
 - Agrandissement des falaises de Knosos au nord-est, qui remplacent l'extrême sud actuel du désert Onyx (et une partie de l'extension projetée de la mer d'Etherium).
 - Agrandissement de la savane de Zobun, à la fois à l'ouest (une bonne partie du désert de Kharma, la bordure de la forêt d'Odhur), au nord (Ygriak ; le bosquet de Flibuste, la plaine de Dastar, une partie du désert de Deliah) et à l'est (le bosquet de Mophort, la plaine de Surindir, une partie du désert de Deliah, et un morceau de savane non nommé jusqu'alors).
 - Agrandissement du marais de Malcheiroso au sud-ouest, pour le connecter à la future extension de la mer d'Etherium (et suppression de ce qui restait de la prairie de Flipp).
 - Suppression de zones naturelles ayant été terraformées (Plaine N) ou allant bientôt l'être (FFMassif01).
 - Fusion des massifs O, Q et H (respectivement au sud-ouest/sud/est de Procyon) en une seule chaîne (XXMassifH), qui sera terraformée dans le futur.
 - L'ID XXMassifQ a été attribué à la barre de montagnes située au nord de Procyon.

## 2020-05-15 (47c4aedd) - Version 2.01
### TCH :
 - Ajout du RER sur le calque de TCH.
 - Ajout du câble A et du projet de câble B sur le calque de TCH.
 - Ajout de l'icône de station du métro 6 (*station/6.svg*), du RER A (*A.svg*/*tA.svg*), des câbles (*ca.svg*/*cb.svg*), de la correspondance triple sans terminus (*x_x_x.svg*), ainsi qu'avec le terminus du câble B (*x_cb.svg*), des correspondances RER sans terminus avec câble A (*Xr_ca*) et générique (*Xr_x*).
 - Ajout de "bâtonnets" de correspondance, pour lier plusieurs gares et/ou stations visuellement séparées (*c.svg*/*cS.svg*).
 - Mise en pointillés des lignes de métro et RER non construites à l'heure actuelle.
 - Modification des tracés des lignes de métro 8, 8bis, 7 et 5b pour coller à l'état construit.
 - Modification du tracé des lignes 6 et 7bis pour coller à l'état prévu.
 - Diminution de la taille de l'icône *station/A_8.svg* (Terminus RER A + M8).
### Villes et lieux-dits :
 - Ajout de Procyon, du Donjon des Ténèbres et de Quatre Chemins.
 - Mise à jour des limites du Hameau, du château d'Illysia, de Midès, du Récif, de Villonne, de Béothas, de Quécol-en-Drie, de Litoréa et de Thalrion.
 - Renommage de la "Base nautique du Récif" en "Camp de vacances du Récif".
### Zones naturelles :
 - Fusion de nombreuses chaînes de montagnes pour former les Balchaïes, à leur superficie définitive planifiée.
 - Ajout de baies et rades manquantes : rade de l'île d'Arithmatie, de Procyon, baie de Valdée.
 - Ajout de l'île d'Arithmatie.
 - Suppression de nombreuses zones naturelles (forêts, plaines...) qui avaient été terraformées hors du monde.
### Mise à jour des cartes de tout le territoire ; elles datent désormais de la fin de l'année 63.

## 2020-04-07 (7da226b5) - Version 2.0
 - Première version uploadée sur Git.
 - Pas de changelog pour les versions précédentes.
