// Script pour générer tous les horaires possibles

var node;
var parent;

// Liste des manières d'afficher chaque chiffre
var chiffre = [	//Lignes de 0 à 6 (ID de colonne dans chaque ligne de 0 à 4 car chaque lettre/chiffre fait 5x7)
	/* 0 */	[[2],			[1,3],	[0,4],		[0,4],		[0,4],		[1,3],	[2]],
	/* 1 */	[[2],			[1,2],	[2],		[2],		[2],		[2],	[1,2,3]],
	/* 2 */	[[1,2,3],		[0,4],	[4],		[3],		[2],		[1],	[0,1,2,3,4]],
	/* 3 */	[[0,1,2,3,4],	[4],	[3],		[2,3],		[4],		[0,4],	[1,2,3]],
	/* 4 */	[[3],			[2,3],	[1,3],		[0,3],		[0,1,2,3,4],[3],	[3]],
	/* 5 */	[[0,1,2,3,4],	[0],	[0,1,2,3],	[4],		[4],		[0,4],	[1,2,3]],
	/* 6 */	[[2,3,4],		[1],	[0],		[0,1,2,3],	[0,4],		[0,4],	[1,2,3]],
	/* 7 */	[[0,1,2,3,4],	[4],	[4],		[3],		[2],		[1],	[0]],
	/* 8 */	[[1,2,3],		[0,4],	[0,4],		[1,2,3],	[0,4],		[0,4],	[1,2,3]],
	/* 9 */	[[1,2,3],		[0,4],	[0,4],		[1,2,3,4],	[4],		[3],	[0,1,2]],
	/* X */ [[0,4],			[0,4],	[1,3],		[2],		[1,3],		[0,4],	[0,4]],
	/* + */ [[],			[2],	[2],		[0,1,2,3,4],[2],		[2],	[]],
	/* . */ [[],			[],		[],			[],			[],			[],		[2]]
];
var texte = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "X", "+", "n"];
var combos = [[0,0], [0,1], [0,2], [0,3], [0,4], [0,5], [0,6], [0,7], [0,8], [0,9],
			  [1,0], [1,1], [1,2], [1,3], [1,4], [1,5], [1,6], [1,7], [1,8], [1,9],
			  [2,0], [2,1], [2,2], [2,3], [2,4], [2,5], [2,6], [2,7], [2,8], [2,9],
			  [3,0], [3,1], [3,2], [3,3], [3,4], [3,5], [3,6], [3,7], [3,8], [3,9],
			  [4,0], [4,1], [4,2], [4,3], [4,4], [4,5], [4,6], [4,7], [4,8], [4,9],
			  [5,0], [5,1], [5,2], [5,3], [5,4], [5,5], [5,6], [5,7], [5,8], [5,9],
			  [6,0], [10,10], [11,11], [12,12]];

// Générer toutes les possibilités de chiffres, de 00 à 99
function gen(){
	// On sauvegarde une référence au panneau horaire "par défaut" (toutes ses lumières sont à l'état off, ainsi qu'au parent de ce noeud
	node = document.getElementById('off');
	parent = document.getElementById('lignes');
	// On sauvegarde également les paramètres de la viewbox (taille de chaque image)
	var viewBox = document.getElementById('PID_timer').getAttribute('viewBox').split(' ');
	var width = parseInt(viewBox[2]);
	var height = parseInt(viewBox[3]);
	
	// On boucle ensuite : pour chaque unité et chaque dizaine
	// (on utilisera ensuite chaque valeur pour connaître les lampes à allumer, en regardant dans le tableau "chiffre" ci dessus)
	for(var combo = 0, cMax = combos.length; combo < cMax; ++combo){
		var diz = combos[combo][0];
		var unit = combos[combo][1];
		
		var clone = node.cloneNode(true);			// We first clone the node
		parent.appendChild(clone);					// We then append it to the parent
		
		clone.setAttribute('id','on_'+texte[diz]+''+texte[unit]);	// We give a unique ID to the clone
		if(diz < 6 || unit == 0){
			clone.setAttribute('transform','translate('+(unit*width)+','+((diz+1)*height)+')');	// We position the clone according to the displayed number
		}
		else{
			clone.setAttribute('transform','translate('+((unit-3)*width)+','+(7*height)+')');
		}
		
		// On itère à travers la liste des lampes à allumer pour chaque chiffre, celui des dizaines puis celui des unités
		for(var l = 0; l < 7; ++l){ for(var c = 0, len = chiffre[diz][l].length; c < len; ++c){ set_light_on(clone,1,l,chiffre[diz][l][c]); }}
		for(var l = 0; l < 7; ++l){ for(var c = 0, len = chiffre[unit][l].length; c < len; ++c){ set_light_on(clone,2,l,chiffre[unit][l][c]); }}
		console.log("Done " + diz + "" + unit + " !");
	}
}

function set_light_on(clone, lettre, ligne, colonne){
	var light = clone.getElementsByClassName('lettre'+lettre)[0].getElementsByClassName('line'+(ligne+1))[0].children[colonne];
	light.classList.add('on');
	light.classList.remove('off');
	light.setAttribute('r','4');
}