// Variables
var scrollZone;

var y = 0;
var yMin = 0;
var yStep;
var x = 0;
var xMax = 2560;
var xStep;

// Fonctions utilitaires
function lastElement(arr){ return arr[arr.length-1]; }


// Fonctions de scroll
function wheel(e){
	if(!e){return;}
	if(e.ctrlKey){return;}
	e.preventDefault();
	e.stopPropagation();
	
	var delta = Math.sign(e.deltaY);
	if(delta < 0){wheelUp(e);}
	if(delta > 0){wheelDown(e);}
}
// Scroll vers le HAUT (on remonte en arrière sur la ligne)
function wheelUp(e){
	e.preventDefault();
	e.stopPropagation();
	y=Math.min(0,y+yStep);
	setScroll();
}
// Scroll vers le BAS (on avance sur la ligne)
function wheelDown(e){
	e.preventDefault();
	e.stopPropagation();
	y=Math.max(yMin,y-yStep);
	setScroll();
}

// Changement de type de panneau (gauche/droite)
function keydown(e){
	if(e.ctrlKey){return;}
	if(e.key == 'ArrowLeft'){
		e.preventDefault();
		e.stopPropagation();
		if(x > 0){ x-=xStep; }
		setScroll();
	}
	else if(e.key == 'ArrowRight'){
		e.preventDefault();
		e.stopPropagation();
		if( x < xMax){ x+=xStep; }
		setScroll();
	}
	else if(e.key == 'ArrowUp'){ wheelUp(e); }
	else if(e.key == 'ArrowDown'){ wheelDown(e); }
}
function setScroll(){
	scrollZone.setAttribute('transform', 'translate(' + (-x) + ',' + y + ')');
}


// Fonction d'initialisation
function onLoad(){	
	scrollZone = document.getElementById('lignes');
	
	var isTimer = document.getElementById('PID_timer');
	var viewbox = ( isTimer != null ? isTimer : document.getElementById('PID') ).getAttribute('viewBox').split(' ');
	xStep = parseInt(viewbox[2])
	yStep = parseInt(viewbox[3])
	
	yMin = -parseInt(lastElement(scrollZone.getElementsByClassName('iterable')).getAttribute('transform').split(',')[1].split(')')[0]);
	xMax = parseInt(lastElement(scrollZone.getElementsByClassName('iterable')).getAttribute('transform').split(',')[0].split('(')[1]);
	
	document.addEventListener("wheel", wheel, {capture:false,passive:false});
	document.addEventListener("keydown", keydown, {capture:false,passive:false});
}