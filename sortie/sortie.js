// Variables
var scrollZone;
var scrollZoneFleche;

var y = 0;
var yMin = 0;
var yStep;
var x = 0;
var xMax = 2560;
var xStep;

var yFleche = 0;
var yFlecheMin = 0;
var xFlecheMax = 1024;

// Fonctions utilitaires
function lastElement(arr){ return arr[arr.length-1]; }


// Fonctions de scroll
function wheel(e){
	if(!e){return;}
	e.preventDefault();
	e.stopPropagation();
	
	var delta = Math.sign(e.deltaY);
	if(delta < 0){wheelUp(e);}
	if(delta > 0){wheelDown(e);}
}
// Scroll vers le HAUT (on remonte en arrière sur la ligne)
function wheelUp(e){	
	if(e.ctrlKey){	yFleche = Math.min(0, yFleche+yStep); }
	else{			y=Math.min(0,y+yStep); }
	setScroll();
}
// Scroll vers le BAS (on avance sur la ligne)
function wheelDown(e){	
	if(e.ctrlKey){	yFleche = Math.max(yFlecheMin, yFleche-yStep); }
	else{			y=Math.max(yMin,y-yStep); }
	setScroll();
}
// Scroll TOUT EN HAUT
function begin(e){	
	y=0;
	setScroll();
}
// Scroll TOUT EN BAS
function end(e){
	y=yMin;
	setScroll();
}

// Changement de type de panneau (gauche/droite)
function keydown(e){
	if(e.key == 'ArrowLeft'){
		e.preventDefault();
		e.stopPropagation();
		if(x > 0){ x-=xStep; }
		setScroll();
	}
	else if(e.key == 'ArrowRight'){
		e.preventDefault();
		e.stopPropagation();
		if( x < xMax){ x+=xStep; }
		setScroll();
	}
	else if(e.key == 'ArrowUp'){ wheelUp(e); }
	else if(e.key == 'ArrowDown'){ wheelDown(e); }
	else if(e.key == 'PageUp'){ begin(e); }
	else if(e.key == 'PageDown'){ end(e); }
}
function setScroll(){
	scrollZone.setAttribute('transform', 'translate(' + (-x) + ',' + y + ')');
	scrollZoneFleche.setAttribute('transform', 'translate(' + (-x%xFlecheMax) + ',' + yFleche + ')');
}


// Fonction d'initialisation
function onLoad(){	
	scrollZone = document.getElementById('lignes');
	scrollZoneFleche = document.getElementById('fleches');
	
	var viewbox = document.getElementById('Sortie').getAttribute('viewBox').split(' ')
	xStep = parseInt(viewbox[2])
	yStep = parseInt(viewbox[3])
	
	yMin = -parseInt(lastElement(scrollZone.getElementsByTagName('g')).getAttribute('transform').split(',')[1].split(')')[0]);
	yFlecheMin = -parseInt(lastElement(scrollZoneFleche.getElementsByClassName('fleche')).getAttribute('transform').split(',')[1].split(')')[0]);
	xFlecheMax = parseInt(lastElement(scrollZoneFleche.getElementsByClassName('fleche')).getAttribute('transform').split(',')[0].split('(')[1]) + xStep;
	
	document.addEventListener("wheel", wheel, {capture:false,passive:false});
	document.addEventListener("keydown", keydown, {capture:false,passive:false});
}