@echo off
setlocal EnableDelayedExpansion
 
if [%1]==[] (set "resolution=256") else (set "resolution=%1")
set "string="
for /L %%y in (-15, 1, 14) do (
	for /L %%x in (-9, 1, 8) do (
		if exist %resolution%\%%x_%%y.png (set "string=!string! %resolution%\%%x_%%y.png") else (set "string=!string! black.png")
	)
)

magick montage -mode concatenate -tile 18x -background black!string! image_%resolution%_full.png