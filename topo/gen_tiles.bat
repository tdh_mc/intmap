@echo off
setlocal EnableDelayedExpansion

if not exist 256\NUL mkdir 256
if not exist 128\NUL mkdir 128
if not exist 64\NUL mkdir 64

for %%f in (512\*.png) do (
	echo "Generating tile %%~nf"
	magick convert 512\%%~nxf -filter point -resize 256x256 256\%%~nxf
	magick convert 512\%%~nxf -filter point -resize 128x128 128\%%~nxf
	magick convert 512\%%~nxf -filter point -resize 64x64 64\%%~nxf
)