// Variables
var scrollZone;

var y = 0;
var yMin = 0;
var yStep = 256;

// Fonctions utilitaires
function lastElement(arr){ return arr[arr.length-1]; }


// Fonctions de scroll
function wheel(e){
	if(!e){return;}
	
	var delta = Math.sign(e.deltaY);
	if(delta < 0 && y < 0){wheelUp(e);}
	if(delta > 0 && y > yMin){wheelDown(e);}
}
// Scroll vers le HAUT (on remonte en arrière sur la ligne)
function wheelUp(e){
	e.preventDefault();
	e.stopPropagation();
	
	y=Math.min(0,y+yStep);
	scrollZone.setAttribute('transform', 'translate(0,' + y + ')');
}
// Scroll vers le BAS (on avance sur la ligne)
function wheelDown(e){
	e.preventDefault();
	e.stopPropagation();
	
	y=Math.max(yMin,y-yStep);
	scrollZone.setAttribute('transform', 'translate(0,' + y + ')');
}

// Fonction de recherche
function keydown(e){
	if(e.key >= 'a' && e.key <= 'z'){
		var tags = scrollZone.getElementsByTagName('g');
		for(var i = 0; i < tags.length; ++i){
			if(tags[i].getAttribute('id')[0] == e.key){
				y = -parseInt(tags[i].getAttribute('transform').substring(12));
				scrollZone.setAttribute('transform', 'translate(0,' + y + ')');
				break;
			}
		}
	}
}


// Fonction d'initialisation
function onLoad(){
	scrollZone = document.getElementById('totems');
	yMin = -parseInt(lastElement(scrollZone.getElementsByTagName('g')).getAttribute('transform').substring(12));
	
	document.addEventListener("wheel", wheel, {capture:false,passive:false});
	document.addEventListener("keydown", keydown, {capture:false,passive:false});
}