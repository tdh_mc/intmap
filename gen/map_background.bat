@echo off
setlocal EnableDelayedExpansion

for /l %%x in (-56, 8, 56) do (
	set /a xMap = 128 * %%x
	set /a xCoord = !xMap! - 64
	if %%x LSS 0 (set /a xID = %%x * -1) else (set /a xID = %%x)
	if !xMap! LSS 0 (set /a tMapIDx = 3) else (set /a tMapIDx = 1)
	
	for /l %%z in (-56, 8, 56) do (
		set /a zMap = 128 * %%z
		set /a zCoord = !zMap! - 64
		if %%z LSS 0 (set /a zID = %%z * -1) else (set /a zID = %%z)
		if !zMap! LSS 0 (set /a tMapID = !tMapIDx! + 1) else (set /a tMapID = !tMapIDx!)
		
		set /a id1 = !tMapID! * 1000000
		set /a id2 = !xID! * 1000
		set /a id3 = !id2! + !zID!
		set /a id = !id1! + !id3!
		
		echo "Processing map !id! (%%x,%%z of 56,56)"
		
		 (@echo 			^<image class="mapBit" id="mapBit_X!xMap!_Y!zMap!"
		@echo 				value="!id!" width="1024" height="1024" x="!xCoord!" y="!zCoord!" /^>) >> "svg_output.txt"
	)
)