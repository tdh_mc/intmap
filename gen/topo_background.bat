@echo off
setlocal EnableDelayedExpansion

for /l %%x in (-15, 1, 15) do (
	set /a xMap = %%x * 512
	
	for /l %%z in (-15, 1, 15) do (
		set /a zMap = %%z * 512
		
		echo "Processing tile %%x,%%z at (!xMap!,!zMap!)"
		
		 (@echo 			^<image class="topoBit" id="topoBit_!xMap!_Y!zMap!"
		@echo 				value="%%x_%%z" width="512" height="512" x="!xMap!" y="!zMap!" /^>) >> "topo_output.txt"
	)
)