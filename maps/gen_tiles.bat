@echo off
setlocal EnableDelayedExpansion

if not exist 512\NUL mkdir 512
if not exist 256\NUL mkdir 256
if not exist 128\NUL mkdir 128
if not exist 64\NUL mkdir 64

for %%f in (1024\*.png) do (
	echo "Generating tile %%~nf"
	magick convert 1024\%%~nxf -filter point -resize 512x512 512\%%~nxf
	magick convert 1024\%%~nxf -filter point -resize 256x256 256\%%~nxf
	magick convert 1024\%%~nxf -filter point -resize 128x128 128\%%~nxf
	magick convert 1024\%%~nxf -filter point -resize 64x64 64\%%~nxf
)