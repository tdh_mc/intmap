<div align="center">

# Carte interactive

### Version web vectorielle

<img src="assets/favicon.png" width="100" height="100">

</div>

## Utilisation

Lancer le fichier `index.html` avec votre navigateur web.

## Changements

Vous pouvez consulter [le changelog](CHANGELOG.md).

