// Variables
var totemID = 0;		// L'id du totem actuellement visible
var totems = undefined;	// La liste de tous les totems

var viewbox = {x:0, y:0, w:256, h:512};

// Utility
// Récupère le dernier mot (par exemple lastWord("Je m'appelle Dereck") => "Dereck")
function lastWord(str){ return str.slice(Math.max(0, str.lastIndexOf(' '))); }
function withoutLastWord(str){ return str.slice(0, Math.max(0, str.lastIndexOf(' '))); }


// Initialisation
function onLoad(){
	// On enregistre les valeurs de la viewbox au cas où elle n'ait pas la valeur par défaut
	var vbArr = document.getElementById('MetroTotem').getAttribute('viewBox').split(' ');
	if(vbArr.length != 4){ console.log('Erreur: Viewbox du document incorrecte.');return; }
	viewbox.x = parseInt(vbArr[0]);
	viewbox.y = parseInt(vbArr[1]);
	viewbox.w = parseInt(vbArr[2]);
	viewbox.h = parseInt(vbArr[3]);
	
	
	// On récupère la liste des totems
	totems = document.getElementsByClassName('totem');
	
	// On calcule les variables pour chacun des totems
	for(var i = 0; i < totems.length; ++i){
		
		// On déplace le totem horizontalement en fonction de sa position
		totems[i].setAttribute('transform', 'translate(' + (256 * i) + ' 0)');
		
		// On récupère la liste des stations de ce totem
		var stations = totems[i].getElementsByClassName('station');
		
		// Nombre de stations, station actuelle (toujours 0)
		totems[i].setAttribute('nombre-stations', '' + stations.length);
		totems[i].setAttribute('current-station', '' + 0);
		totems[i].setAttribute('translate-x', '' + (256 * i));
		totems[i].setAttribute('translate-y', '' + 0);
		totems[i].setAttribute('default-y', '' + totems[i].getElementsByClassName('station')[0].getElementsByTagName('image')[0].getAttribute('y'));
		
		// On affiche l'effet visuel de sélection
		stations[0].classList.add('selected');
		
		// On copie le premier "point" de station à l'extérieur de la partie mobile
		// Pour que la flèche reste là quelle que soit la translation
		totems[i].appendChild(totems[i].getElementsByClassName('dot')[0].cloneNode());
	}
	
		
	// On récupère la liste des chemins (lignes de métro/câble)
	// Il y en aura toujours 1 dans le cas général, mais 2 pour les câbles
	var chemins = document.querySelectorAll('path.cable,path.metro');
	
	// On enregistre le Y final de chaque chemin
	for(var i = 0; i < chemins.length; ++i){
		chemins[i].setAttribute('default-y', lastWord(chemins[i].getAttribute('d')));
	}
	// Lorsqu'on translatera la ligne vers le haut, on fera se terminer le chemin de plus en plus tôt
	// Ceci a pour unique but d'empêcher Inkscape de niquer l'export automatique en croyant que le calque est
	//	-- plus grand que 512 de haut (puisqu'un chemin translaté sans modification dépasserait en haut...)
	
	
	
	
	// On répare le style de tous les textes compressés
	var ctext = document.querySelectorAll('.compress1,.compress2,.compress3,.compress4');
	for(var i = 0; i < ctext.length; ++i){
		var x = Math.max(0, parseInt(ctext[i].getAttribute('x')) - 20);
		var y = Math.max(0, parseInt(ctext[i].getAttribute('y')));
		var scaleX = ctext[i].classList.contains('compress1') ? 0.88 :
					(ctext[i].classList.contains('compress2') ? 0.8 :
					(ctext[i].classList.contains('compress3') ? 0.66 :
					0.5));
		
		ctext[i].setAttribute('style', 'transform-origin: ' + ctext[i].getAttribute('x') +'px '+ ctext[i].getAttribute('y') + 'px;');
		ctext[i].setAttribute('transform', 'translate(' + x + ' ' + y + ') scale(' + scaleX + ' 1) translate(-' + x + ' -' + y + ')');
	}
	
	
	
	// On ajoute les event listeners
	document.addEventListener('keydown', key);
}




// Les fonctions sa mère
function key(e){
	if(e==null){return;}
	
	if(e.key=='ArrowLeft'){changeTotem(-1,e);}
	if(e.key=='ArrowRight'){changeTotem(1,e);}
	if(e.key=='ArrowUp'){changeStation(-1,totems[totemID],e);}
	if(e.key=='ArrowDown'){changeStation(1,totems[totemID],e);}
	
	if((e.key=='E'||e.key=='e')&&e.ctrlKey){saveTotems(e);}
}

// Sauvegarde de tous les totems
function saveTotems(event){
	// On stoppe éventuellement la propagation de l'event
	if(event!=null){
		event.preventDefault();
		event.stopPropagation();
	}
	console.log('Saving...');
	
	// On se replace d'abord au niveau du premier totem
	changeTotem(-999, null);
	
	// Ensuite, pour chaque totem, on le copie en faisant des transformations verticales
	for(var i = 0; i < totems.length; ++i){
		var totem = totems[i];
		var stations = totem.getElementsByClassName('station');
		
		// On crée une nouvelle version du totem pour chaque station
		// (sauf la première, donc on commence à 1)
		for(var j = 1; j < stations.length; ++j){
			var totemCopy = totem.cloneNode(true);
			
			// On change son ID, ainsi que sa classe pour éviter d'itérer à l'infini
			totemCopy.setAttribute('id', totemCopy.getAttribute('id') + '\\' + stations[j].getAttribute('id-station'));
			
			totemCopy.classList.remove('totem');
			totemCopy.classList.add('totemCopy');
			
			// On modifie sa transformation en fonction de la station actuelle
			totemCopy.setAttribute('transform', 'translate(' + totemCopy.getAttribute('translate-x') + ' ' + (512 * j) + ')');
			
			// On fait avancer la ligne jusqu'à la station actuelle
			changeStation(j, totemCopy);
			
			// Enfin, on l'ajoute au DOM
			var newTotem = document.getElementById('MetroTotem').appendChild(totemCopy);
			
			// On supprime les stations cachées car trop hautes (pour ne pas dépasser dans Inkscape)
			var hiddenStations = newTotem.getElementsByClassName('hidden');
			for (var k = hiddenStations.length - 1; k >= 0; --k){
				hiddenStations[k].remove();
			}
		}
		
		// On modifie l'ID de la première station
		totem.setAttribute('id', totem.getAttribute('id') + '\\' + stations[0].getAttribute('id-station'));
	}
	
	console.log('Saved! Ctrl+S to download Inkscape file.');
}

// Changement de totem actif
// offset négatif = vers la gauche, positif = vers la droite
function changeTotem(offset, event){
	if(offset==undefined){return;}
	if(event!=null){
		event.preventDefault();
		event.stopPropagation();
	}
	
	// On convertit l'offset de Nombre de totems à Nombre de pixels
	offset *= 256;
	// Si l'offset déborde d'un côté ou de l'autre, on le clamp
	if(viewbox.x + offset < 0){ offset = -viewbox.x; }
	if(viewbox.x + offset >= 256 * totems.length){ offset = (256 * (totems.length - 1)) - viewbox.x; }
	
	// On met à jour les variables
	viewbox.x += offset;
	totemID += Math.floor(offset / 256);
	
	// On met à jour la viewbox du document à partir de la variable
	document.getElementById('MetroTotem').setAttribute('viewBox', '' + viewbox.x + ' ' + viewbox.y + ' ' + viewbox.w + ' ' + viewbox.h);
}

// Changement de station dans le totem actif
// offset négatif = vers le haut (à rebours), positif = vers le bas (sens normal)
function changeStation(offset, totem, event){
	if(offset==undefined){return;}
	if(totem==undefined){totem=totems[totemID];}
	if(event!=null){
		event.preventDefault();
		event.stopPropagation();
	}
	
	// On récupère la ligne (partie mobile) + le(s) chemin(s)
	var ligne = totem.getElementsByClassName('ligne')[0];
	var chemins = ligne.getElementsByTagName('path');
	// On récupère les attributs de notre totem
	var currentStation = parseInt(totem.getAttribute('current-station'));
	var totalStations = parseInt(totem.getAttribute('nombre-stations'));
	var defaultY = parseInt(totem.getAttribute('default-y'));
	var translateY = parseInt(totem.getAttribute('translate-y'));
	// On récupère les éléments de la ligne
	var stations = totem.getElementsByClassName('station');
	var currentY = parseInt(stations[currentStation].getElementsByClassName('dot')[0].getAttribute('y'));
	
	// On clamp l'offset au cas où il déborde
	if(currentStation + offset < 0) { offset = -currentStation; }
	if(currentStation + offset >= totalStations) { offset = totalStations - 1 - currentStation; }
	
	// On retire le rectangle bleu autour de la station
	stations[currentStation].classList.remove('selected');
	
	// On modifie la station actuelle
	currentStation += offset;
	// On calcule la différence de position nécessaire pour atteindre la station de destination
	var deltaY = stations[currentStation].getElementsByClassName('dot')[0].getAttribute('y') - currentY;
	// On applique cette différence de position
	translateY -= deltaY;
	
	
	// On applique la nouvelle translation à la ligne
	ligne.setAttribute('transform', 'translate(0 ' + translateY + ')');
	// On raccourcit les chemins si nécessaire
	for(var i = 0; i < chemins.length; ++i){
		chemins[i].setAttribute('d', withoutLastWord(chemins[i].getAttribute('d')) + ' ' + (parseInt(chemins[i].getAttribute('default-y')) - Math.min(0, translateY + 50)));
	}
	
	// On modifie l'affichage des stations selon leur position
	stations[currentStation].classList.add('selected');							// La nouvelle devient sélectionnée
	for(var i=0; i<currentStation; ++i){ stations[i].classList.add('hidden'); } // Celles + haut deviennent cachées
	for(var i=currentStation; i<totalStations; ++i){
		stations[i].classList.remove('hidden'); // Nous, ainsi que celles + bas que nous, devenons visibles
	}
	
	
	// On enregistre les variables modifiées
	totem.setAttribute('current-station', currentStation);
	totem.setAttribute('translate-y', translateY);
}