var svg = document.getElementById('TerresDuHameauMap');
var defaultWidth;
var defaultHeight;
var defaultViewbox;
var posFrom;
var posTo;
var pt;

function posmod(n,d){ return ((n%d)+d)%d; }

var interfaceLayer;
var miniatureLayer;
var rechercheLayer;
var rechercheResultats;
var rechercheFocus = false;
var rechercheCurseurPos = 0;
var rechercheResultatsMaxOffset;
var rechercheResultatsOffset = 0;

var legende;
var legendeFleche;
var hoverInfoLayer;
var hoverInfoRect;
var hoverInfoText;

var infobulle;
var infobullePin;
var infobulleLink;
var infobulleText;
var infobulleSmallSub;
var infobulleBigSub;

var mapParent;
var mapElement;
var regionsLayer;
var tchLayer;
var routesLayer;
var fleuvesLayer;
var villesLayer;
var lieuxDitsLayer;
var grilleLayer;
var projetsLayer;
var geoLayer;
var coordsLayer;
var pinLayer;

var coordsText;
var mapIDText;

var clickX = -1;
var clickY = -1;
var offsetX = 0;
var offsetY = 0;
var sizeX = 0;
var sizeY = 0;
var realSizeX = 0;
var realSizeY = 0;
var lastClientX = -1;
var lastClientY = -1;

var computedTileHeight = 0;
var displayedTileHeight = 512;
var tileHeightChangeTimeout;

var fixedPoint = null;
var originalOffset = null;

var clickMoved = false;
var lastStillClick = 0;

var moveLR = null;
var moveUD = null;

var grilleSpacing = 128;
var grilleHoriz;
var grilleVert;

var fondCarteShown = true;
var regionsShown = false;
var metroShown = false;
var villesShown = true;
var lieuxDitsShown = true;
var commercesShown = true;
var routesShown = true;
var fleuvesShown = true;
var picsShown = true;
var coordsShown = false;
var grilleShown = false;
var projetsShown = false;
var geoShown = true;
var topoShown = false;
var legendShown = false;
var rechercheShown = 0;
var positionShown = false;
function isLayerVisible(id){
	switch(id){
		case 'fondCarte':			return fondCarteShown;
		case 'calqueGeographie':	return geoShown;
		case 'calqueTopographie':	return topoShown;
		case 'calqueRegions':		return regionsShown;
		case 'calqueVilles':		return villesShown;
		case 'calqueCoursEau':		return fleuvesShown;
		case 'calquePics':			return picsShown;
		case 'calqueRoutes':		return routesShown;
		case 'calqueLieuxDits':		return lieuxDitsShown;
		case 'calqueCommerces':		return commercesShown;
		case 'calqueProjets':		return projetsShown;
		case 'calqueTCH':			return metroShown;
		case 'grille':				return grilleShown;
		case 'positionnement':		return positionShown;
	}
	return false;
}

var doubleResolution = false;

var smallPathsShown = true;
var mediumPathsShown = true;
var bigPathsShown = true;

var smallPaths;
var mediumPaths;
var bigPaths;

var mapBits = null;
var mapBitIndex = 0;
var mapBitOffset = 0;
var topoBits = null;
var topoBitIndex = 0;
var topoSecondaryBg = null;

var boolCenterOnCurrent;


var miniatures = {
	'VilleArgencon': 'miniature/ville/argencon.png',
	'VilleAsvaard': 'miniature/ville/asvaard.png',
	'VilleAulnoy': 'miniature/ville/aulnoy.png',
	'VilleBeothas': 'miniature/ville/beothas.png',
	'VilleBussyNigel': 'miniature/ville/bussy_nigel.png',
	'VilleCalmeflot': 'miniature/ville/calmeflot.png',
	'VilleChassy': 'miniature/ville/chassy.png',
	'VilleDorlinor': 'miniature/ville/dorlinor.png',
	'VilleDuerrom': 'miniature/ville/duerrom.png',
	'VilleEvrocq': 'miniature/ville/evrocq.png',
	'VilleFortDuVal': 'miniature/ville/fort_du_val.png',
	'VilleFrambourg': 'miniature/ville/frambourg.png',
	'VilleGeorlie': 'miniature/ville/georlie.png',
	'VilleGrenat': 'miniature/ville/grenat.png',
	'VilleHades': 'miniature/ville/hades.png',
	'VilleDodene': 'miniature/ville/la_dodene.png',
	'VilleHameau': 'miniature/ville/le_hameau.png',
	'VilleSablons': 'miniature/ville/les_sablons.png',
	'VilleLitorea': 'miniature/ville/litorea.png',
	'VilleMides': 'miniature/ville/mides.png',
	'VilleMilloreau': 'miniature/ville/milloreau.png',
	'VilleMoronia': 'miniature/ville/moronia.png',
	'VilleNeronil': 'miniature/ville/neronil.png',
	'VilleProcyon': 'miniature/ville/procyon.png',
	'VilleQuecol': 'miniature/ville/quecol.png',
	'VilleTernelieu': 'miniature/ville/ternelieu.png',
	'VilleThalrion': 'miniature/ville/thalrion.png',
	'VilleTolbrok': 'miniature/ville/tolbrok.png',
	'VilleVerchamps': 'miniature/ville/verchamps.png',
	'VilleVillonne': 'miniature/ville/villonne.png',
	'VilleYgriak': 'miniature/ville/ygriak.png',
	
	'LDTempleChizan': 'miniature/lieu_dit/chizan.png',
	'LDCitadelleGzor': 'miniature/lieu_dit/citadelle_gzor.png',
	'LDDonjonTenebres': 'miniature/lieu_dit/donjon_tenebres.png',
	'LDGrenatRecif': 'miniature/lieu_dit/grenat_recif.png',
	'LDChateauIllysia': 'miniature/lieu_dit/illysia.png',
	'LDCampMinierTotem': 'miniature/lieu_dit/mines_totem.png',
	'LD4Chemins': 'miniature/lieu_dit/quatre_chemins.png',
	'LDSanctuaireTemps': 'miniature/lieu_dit/sanctuaire_temps.png',
	
	'MetroHameau': 'miniature/station/le_hameau_rive_gauche.png',
	'RERHameau': 'miniature/station/le_hameau_rive_droite.png',
	'MetroHameauGbrnNord': 'miniature/station/le_hameau_glandebruine_nord.png',
	'MetroVerchamps': 'miniature/station/verchamps.png',
	'MetroKraken': 'miniature/station/kraken.png',
	'MetroKnosos': 'miniature/station/knosos.png',
	'MetroZobunEclesta': 'miniature/station/zobun_eclesta.png',
	'MetroEvenisEclesta': 'miniature/station/evenis_eclesta.png',
	'Metro4Chemins': 'miniature/station/quatre_chemins.png',
	'MetroSablons': 'miniature/station/les_sablons.png',
	'MetroChizan': 'miniature/station/chizan.png',
	'MetroIllysia': 'miniature/station/illysia.png',
	'RERVillonne': 'miniature/station/villonne.png',
	'RERGrenat': 'miniature/station/grenat_ville.png',
	'MetroKrypt': 'miniature/station/montagnes_de_krypt.png',
	'MetroFultez': 'miniature/station/fultez.png',
	'MetroPieuze': 'miniature/station/pieuze.png',
	'MetroAsvaardRudelieu': 'miniature/station/asvaard_rudelieu.png',
	'MetroOnyx': 'miniature/station/desert_onyx.png',
	'MetroAlwinOnyx': 'miniature/station/alwin_onyx.png',
	'MetroYgriak': 'miniature/station/ygriak.png',
	'MetroDorlinor': 'miniature/station/dorlinor.png',
	'MetroLaar': 'miniature/station/desert_laar.png',
	'MetroDuerrom': 'miniature/station/duerrom.png',
	'MetroThalrion': 'miniature/station/thalrion.png',
	'MetroLitorea': 'miniature/station/litorea.png',
	'MetroTolbrok': 'miniature/station/tolbrok.png',
	'MetroChassy': 'miniature/station/chassy.png',
	'REREvrocq': 'miniature/station/evrocq_sauvebonne.png',
	'MetroAulnoy': 'miniature/station/aulnoy.png',
	'MetroNeronil': 'miniature/station/neronil.png',
	'MetroMoronia': 'miniature/station/moronia.png',
	'MetroQuecol': 'miniature/station/quecol.png',
	'MetroFrambourgTernelieu': 'miniature/station/frambourg_ternelieu.png',
	'MetroGeorlie': 'miniature/station/georlie.png',
	'MetroCalmeflot': 'miniature/station/calmeflot.png',
	'MetroMilloreau': 'miniature/station/milloreau.png',
	'MetroBussyNigel': 'miniature/station/bussy_nigel.png',
	'MetroArgencon': 'miniature/station/argencon.png',
	'CableDodene': 'miniature/station/la_dodene.png',
	'CablePics': 'miniature/station/les_pics.png',
	'RERProcyon': 'miniature/station/procyon.png',
	'MetroTenebres': 'miniature/station/procyon_tenebres.png',
	'MetroFortDuVal': 'miniature/station/fort_du_val.png',
	'MetroHades': 'miniature/station/hades.png',
	'MetroMides': 'miniature/station/mides.png'
};


function toggleLayer(id){
	var element = document.getElementById(id);
	if(element == null){console.log('' + id + ' is null.'); return;}
	
	var newState = true;
	switch(id){
		case 'fondCarte':			fondCarteShown = !fondCarteShown; newState = fondCarteShown; break;
		case 'calqueTopographie':	topoShown= !topoShown; newState = topoShown; break;
		case 'calqueGeographie':	geoShown = !geoShown; newState = geoShown; break;
		case 'calqueRegions':		regionsShown = !regionsShown; newState = regionsShown; break;
		case 'calqueVilles':		villesShown = !villesShown; newState = villesShown; break;
		case 'calqueCoursEau':		fleuvesShown = !fleuvesShown; newState = fleuvesShown; break;
		case 'calquePics':			picsShown = !picsShown; newState = picsShown; break;
		case 'calqueRoutes':		routesShown = !routesShown; newState = routesShown; break;
		case 'calqueLieuxDits':		lieuxDitsShown = !lieuxDitsShown; newState = lieuxDitsShown; break;
		case 'calqueCommerces':		commercesShown = !commercesShown; newState = commercesShown; break;
		case 'calqueProjets':		projetsShown = !projetsShown; newState = projetsShown; break;
		case 'calqueTCH':			metroShown = !metroShown; newState = metroShown; break;
		case 'grille':				grilleShown = !grilleShown; newState = grilleShown; break;
		case 'positionnement':		positionShown = !positionShown; newState = positionShown; break;
		default: newState = false;
	}
	var legendeToggle = document.getElementById('legendeToggle_' + id);
	if(newState){
		element.style.animationName = 'fadeIn';
		element.style.display = 'inline';
		if(legendeToggle != null){ legendeToggle.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', 'icon/checkbox_on.svg'); }
	}
	else{
		element.style.animationName = 'fadeOut';
		setTimeout(hideLayer, 600, element);
		if(legendeToggle != null){ legendeToggle.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', 'icon/checkbox_off.svg'); }
	}
}
function hideLayer(element){ element.style.display = 'none'; }

function leadingZeroes(number, displayLength){
	var absNumber = Math.abs(number).toString();
	if(number < 0){
		return "-"+(("000000000000"+absNumber).substr(-displayLength));
	}
	else{
		return ("00000000000000"+absNumber).substr(-displayLength);
	}
}


function toggleLegende(){
	legendShown = !legendShown;
	if (legendShown) {
		legende.style.animationName = 'legendeAnimationIn';
		legende.style.animationPlayState = 'running';
		legendeFleche.style.animationName = 'legendeFlecheAnimationIn';
		legendeFleche.style.animationPlayState = 'running';
	}
	else{
		legende.style.animationName = 'legendeAnimationOut';
		legende.style.animationPlayState = 'running';
		legendeFleche.style.animationName = 'legendeFlecheAnimationOut';
		legendeFleche.style.animationPlayState = 'running';
	}
}
function toggleRecherche(){
	if (rechercheShown == 0) {
		rechercheLayer.style.animationName = 'rechercheAnim_noneToSmall';
		rechercheLayer.style.animationPlayState = 'running';
		rechercheShown = 1;
	}
	else{
		if (rechercheShown == 1) {	rechercheLayer.style.animationName = 'rechercheAnim_smallToNone'; }
		else{ 						rechercheLayer.style.animationName = 'rechercheAnim_bigToNone'; }
		rechercheLayer.style.animationPlayState = 'running';
		rechercheShown = 0;
	}
}

function addHoverInfo(element){
	element.addEventListener("mouseenter", hoverButton, {capture:false,passive:false});
	element.addEventListener("mouseleave", unhoverButton, {capture:false,passive:false});
}
function hoverButton(e){
	var newText = e.target.getAttribute('hoverInfo');
	
	// On vérifie que le texte à afficher existait bien
	if(newText == null){ return; }
	if(newText == hoverInfoText.innerHTML){ return; }
	
	// Si oui, on assigne le nouveau texte
	hoverInfoText.innerHTML = newText;
	var textSize = hoverInfoText.getBBox().width;
	hoverInfoRect.setAttribute('width', '' + (16 + textSize));
	
	// On rend ensuite l'infobulle visible
	hoverInfoLayer.setAttribute('transform', 'translate(' + (e.clientX - (e.clientX + textSize > svg.getAttribute('width') ? (textSize + 24) : 0)) + ' ' + e.clientY + ')');
	hoverInfoLayer.style.animation = 'hoverInfoIn .5s ease-out .9s 1 normal forwards';
}
function unhoverButton(e){
	// On supprime le texte
	hoverInfoText.innerHTML = '';
	// On rend ensuite l'infobulle invisible
	hoverInfoLayer.style.animation = 'hoverInfoOut .5s ease-in 0s 1 normal forwards';
}


function initGrille(){
	var bb = mapParent.getBBox();
	bb.x -= posmod((bb.x + mapBitOffset), grilleSpacing);
	bb.y -= posmod((bb.y + mapBitOffset), grilleSpacing);
	bb.width += grilleSpacing - posmod((bb.x + mapBitOffset), grilleSpacing);
	bb.height += grilleSpacing - posmod((bb.x + mapBitOffset), grilleSpacing);
	
	var x = bb.x;
	var xMax = bb.x + bb.width;
	var y = bb.y;
	var yMax = bb.y + bb.height;
	
	var horizontalPath = 'M ' + x + ' ' + y;
	var verticalPath = 'M ' + x + ' ' + y;
	
	while(y < yMax){
		if(x == xMax) { x = bb.x; }
		else{ x = xMax; }
		y += grilleSpacing;
		horizontalPath += (' H ' + x + ' V ' + y);
	}
	
	x = bb.x;
	while(x < xMax){
		if(y == yMax) { y = bb.y; }
		else{ y = yMax; }
		x += grilleSpacing;
		verticalPath += (' V ' + y + ' H ' + x);
	}
	
	grilleHoriz.setAttribute('d', horizontalPath);
	grilleVert.setAttribute('d', verticalPath);
	grilleHoriz.style.strokeWidth = '' + (grilleSpacing / 32) + 'px';
	grilleVert.style.strokeWidth = '' + (grilleSpacing / 32) + 'px';
}

function mousePosition(e){
	pt.x = e.clientX;
    pt.y = e.clientY;
	pt = pt.matrixTransform(mapElement.getScreenCTM().inverse());
	
	document.getElementById('mousePosX').setAttribute('value',pt.x);
	document.getElementById('mousePosY').setAttribute('value',pt.y);
}
var delta = {x:0,y:0};
function mouseMovement(e){
	delta.x = e.movementX;
    delta.y = e.movementY;
	var sizeFactor = sizeX / svg.getAttribute('width');
	delta.x *= sizeFactor;
	delta.y *= sizeFactor;
}

function updateViewbox(){
	viewBox = mapParent.getAttribute('viewBox').split(' ');
	for(var i = 0, max = 4; i < max; ++i){viewBox[i]=parseInt(viewBox[i]);}
	
	offsetX = viewBox[0];
	offsetY = viewBox[1];
	sizeX = viewBox[2];
	sizeY = viewBox[3];
}

function globalMouseDown(e){
	if(e.target.getAttribute('id') != null && e.target.getAttribute('id').includes('rechercheChamp')){
		rechercheFocus = true;
		rechercheCurseur.style.display = 'inline'; }
	else{ rechercheFocus = false;
		rechercheCurseur.style.display = 'none'; }
}
function mapMouseDown(e){
	if(!e){return;}
	
	mousePosition(e);
	fixedPoint = {x:pt.x, y:pt.y};
	originalOffset = {x:offsetX, y:offsetY};
	
	clickMoved=false;
	updateViewbox();
	
	e.preventDefault();
	e.stopPropagation();
}

function scaleAt(factor, pointX, pointY){
	pointX = (typeof pointX !== 'undefined') ? pointX : 99999;
	pointY = (typeof pointY !== 'undefined') ? pointY : 99999;
	
	offsetX += (pointX - viewBox[0]) * (1 - factor);
	offsetY += (pointY - viewBox[1]) * (1 - factor);
	sizeX *= factor;
	sizeY *= factor;
	rescale();
}
function mapMouseZoom(e){
	if(!e){return;}
	var delta = e.deltaY;
	
	updateViewbox();
	mousePosition(e);
	
	//var posX = parseInt(svg.getElementById('mousePosX').getAttribute('value'));
	//var posY = parseInt(svg.getElementById('mousePosY').getAttribute('value'));
	console.log(pt.x + " && " + pt.y);
	
	if(delta > 0 && sizeX < 44000 && sizeY < 36000) { scaleAt(1.25, pt.x, pt.y); }
	else if(delta < 0 && sizeX > 80 && sizeY > 67) { scaleAt(0.8, pt.x, pt.y); }
	
	clearTimeout(tileHeightChangeTimeout);
	tileHeightChangeTimeout = setTimeout(testForTileHeightChange, 500);
	
	e.preventDefault();
	return false;
}

function menuMouseMove(e){
	if(fixedPoint != null){mapMouseUp(e);}	// Si on est en train de scroll, on arrête puisqu'on sort de la zone
}
function mapMouseMove(e){
	if(!e){return;}
	
	mouseMovement(e);
	if(fixedPoint != null && (delta.x != 0 || delta.y != 0)){
		offsetX -= delta.x;
		offsetY -= delta.y;
		clickMoved = true;
		rescale();			
	}
}

function clampOffset(){
	if(sizeY > 33280) {sizeX = 33280 * (sizeX / sizeY); sizeY = 33280;}
	if(offsetY + sizeY > 24000) { offsetY = 24000 - sizeY; }
	else if(offsetY < -24000){ offsetY = -24000; }
	if(offsetX + sizeX > 32000) { offsetX = 32000 - sizeX; }
	else if(offsetX < -32000){offsetX = -32000;}
}

function rescale(){
	clampOffset();
	
	var vbArgument = "" + offsetX + " " + offsetY + " " + sizeX + " " + sizeY;
	mapParent.setAttribute('viewBox', vbArgument);
	
	var pinScale = sizeX/60;
	posFrom.style.transform = 'scale('+pinScale+','+pinScale+')';
	posTo.style.transform = 'scale('+pinScale+','+pinScale+')';
}

function mapMouseUp(e){
	if(!e){return;}
	
	// console.log("mouseup");
	if(clickMoved){
		// console.log("click moved");
		e.preventDefault();
		e.stopPropagation();
	}
	else{
		// On affiche les coordonnées dans le cadre en bas à gauche, et on positionne le bon pin
		posFrom.setAttribute('x',Math.round(pt.x-0.5));
		posFrom.setAttribute('y',Math.round(pt.y-0.5)-1);
		coordsText.innerHTML = ''+(Math.round(pt.x-0.5))+' ; '+(Math.round(pt.y-0.5));
		
		// On calcule l'ID IG de la carte correspondant à cette position
		var mapIDx = Math.floor(pt.x - posmod((pt.x + mapBitOffset), 128) + 64) / 128;
		var mapIDy = Math.floor(pt.y - posmod((pt.y + mapBitOffset), 128) + 64) / 128;
		var mapIDn = 1;
		if(mapIDx < 0) { mapIDn+=2; }
		if(mapIDy < 0) { mapIDn++; }
		if(mapIDx > -1000 && mapIDx < 1000 && mapIDy > -1000 && mapIDy < 1000) { mapIDText.innerHTML = 'map_' + mapIDn.toString() + leadingZeroes(Math.abs(mapIDx),3) + leadingZeroes(Math.abs(mapIDy),3); }
		else	{ mapIDText.innerHTML = 'map_???'; }
		
		if(e.button == 0 && e.target && e.target.classList && e.target.classList.length > 0 && e.target.classList.contains('hoverable')){
			clicked(e.target.id, 1);
		}
		else if(e.button == 0 && e.target && e.target.parentNode && e.target.parentNode.classList && e.target.parentNode.classList.length > 0 && e.target.parentNode.classList.contains('hoverable')){
			clicked(e.target.parentNode.id, 1);
		}
		else{
			unclicked();
		}
	}
		
	fixedPoint = null;
}

function mouseLeave(e){
	// console.log("mouseleave");
	mapMouseUp(e);
}

function clickPosition(){	
	document.getElementById('clickPosX').setAttribute('value',pt.x);
	document.getElementById('clickPosY').setAttribute('value',pt.y);
}

function viewboxPosition(x, y, width){
	sizeY = width * (sizeY / sizeX);
	sizeX = width;
	offsetX = x - (sizeX / 2);
	offsetY = y - (sizeY / 2);
	rescale();
}

/* Fonctions relatives à l'affichage des "infobulles",
	dans la barre d'information située en bas de l'interface */
function hideInfobulle(){ showInfobulle(); } /* On appelle la fonction sans argument pour qu'elle se cache */
function showInfobulle(element){	/* L'argument est l'élément SVG hoverable pointé par l'utilisateur */
	/* On essaie de récupérer le nom de l'élément s'il existe */
	var name = element==null? null : element.getAttribute('value');
	
	/* Si on n'a pas trouvé de nom (=pas d'élément ou pas un hoverable), on cache les données affichées */
	if(name == null){
		infobullePin.style.display = 'none';
		infobulleText.innerHTML = '';
		infobulleBigSub.innerHTML = '';
		infobulleSmallSub.innerHTML = '';
		infobulleLink.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', 'https://cgc.terresduhameau.eu.org');
		infobulleLink.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:title', 'Visiter le wiki du CGC');
		getWikiImage(null);
		return;
	}
	
	/* On essaie de récupérer les différentes variables possibles */
	var regions = element.getAttribute('regions'); if(regions!=null){regions=regions.split(' ');}
	var villes = element.getAttribute('villes'); if(villes!=null){villes=villes.split(' ');}
	var stations = element.getAttribute('stations'); if(stations!=null){stations=stations.split(' ');}
	var stationsProjet = element.getAttribute('stations-projet'); if(stationsProjet!=null){stationsProjet=stationsProjet.split(' ');}
	var lignes = element.getAttribute('lignes'); if(lignes!=null){lignes=lignes.split(' ');}
	var lignesProjet = element.getAttribute('lignes-projet'); if(lignesProjet!=null){lignesProjet=lignesProjet.split(' ');}
	
	/* On assigne le nom et le lien */
	infobulleText.innerHTML = name;
	infobulleLink.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', 'https://cgc.terresduhameau.eu.org/wiki/' + name.replaceAll(' ', '_') + '/');
	infobulleLink.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:title', 'Visiter « ' + name + ' » sur le wiki du CGC');
	
	/* On lance la requête pour récupérer une miniature si elle existe */
	getWikiImage(element.getAttribute('id'));
	/* On affiche le pin de positionnement */
	infobullePin.style.display = 'inline';
	
	var bigSubtitleTaken = false;
	var smallSubtitleTaken = false;
	
	// Selon notre classe, on affiche des infos différentes si elles sont disponibles
	if(	element.classList.contains('ligneCable') ||
		element.classList.contains('ligneMetro') ||
		element.classList.contains('ligneBateau') ||
		element.classList.contains('ligneDirigeable') ||
		element.classList.contains('ligneRER')){
			// Si on est une ligne, on affiche en grand les correspondances et en petit les stations desservies
			if(lignes != null){
				// On affiche d'abord les lignes déjà existantes
				tempLignes = (lignes.length == 1 ? "Correspondance avec le " : "Correspondance avec les lignes ");
				for(var i = 0; i < lignes.length; ++i){
					if(i > 0){
						if(i + 1 < lignes.length){ tempLignes = tempLignes.concat(', '); }
						else{ tempLignes = tempLignes.concat(' et '); }
					}
					var ligne = document.getElementById(lignes[i]);
					if(ligne==null){ console.log("Impossible de trouver la ligne d'ID " + lignes[i]); }
					else{ tempLignes = tempLignes.concat('<a onclick="clicked(&quot;' + ligne.getAttribute('id') + '&quot;);" class="infobulleLink">' + ligne.getAttribute('id-ligne') + '</a>'); } // En attendant mieux
				}
				// On ajoute ensuite à la phrase les lignes en projet s'il y en a
				if(lignesProjet != null){
					tempLignes = tempLignes.concat(' (en projet : ');
					for(var i = 0; i < lignesProjet.length; ++i){
						if(i > 0){
							if(i + 1 < lignesProjet.length){ tempLignes = tempLignes.concat(', '); }
							else{ tempLignes = tempLignes.concat(' et '); }
						}
						var ligne = document.getElementById(lignesProjet[i]);
						if(ligne==null){ console.log("Impossible de trouver la ligne d'ID " + lignesProjet[i]); }
						else{ tempLignes = tempLignes.concat('<a onclick="clicked(&quot;' + ligne.getAttribute('id') + '&quot;);" class="infobulleLink">' + ligne.getAttribute('id-ligne') + '</a>'); } // En attendant mieux
					}
					tempLignes = tempLignes.concat(')');
				}
				infobulleSmallSub.innerHTML = tempLignes;
			}
			else if(lignesProjet != null){
				tempLignes = (lignesProjet.length == 1 ? "Correspondance projetée avec le " : "Correspondance projetée avec les lignes ");
				for(var i = 0; i < lignesProjet.length; ++i){
					if(i > 0){
						if(i + 1 < lignesProjet.length){ tempLignes = tempLignes.concat(', '); }
						else{ tempLignes = tempLignes.concat(' et '); }
					}
					var ligne = document.getElementById(lignesProjet[i]);
					if(ligne==null){ console.log("Impossible de trouver la ligne d'ID " + lignesProjet[i]); }
					else{ tempLignes = tempLignes.concat('<a onclick="clicked(&quot;' + ligne.getAttribute('id') + '&quot;);" class="infobulleLink">' + ligne.getAttribute('id-ligne') + '</a>'); } // En attendant mieux
				}
				infobulleSmallSub.innerHTML = tempLignes;
			}
			else{ infobulleSmallSub.innerHTML = 'Pas de lignes en correspondance.'; }
			if(stations != null){
				tempStations = (stations.length == 1 ? "Dessert la station " : "Dessert les stations ");
				for(var i = 0; i < stations.length; ++i){
					if(i > 0){
						if(i + 1 < stations.length){ tempStations = tempStations.concat(', '); }
						else{ tempStations = tempStations.concat(' et '); }
					}
					var station = document.getElementById(stations[i]);
					if(station==null){ console.log("Impossible de trouver la station d'ID " + stations[i]); }
					else{ tempStations = tempStations.concat('<a onclick="clicked(&quot;' + station.getAttribute('id') + '&quot;);" class="infobulleLink">' + station.getAttribute('value').split(' (')[0] + '</a>'); }
				}
				if(stationsProjet != null){
					tempStations = tempStations.concat(' (en projet : ')
					for(var i = 0; i < stationsProjet.length; ++i){
						if(i > 0){
							if(i + 1 < stationsProjet.length){ tempStations = tempStations.concat(', '); }
							else{ tempStations = tempStations.concat(' et '); }
						}
						var station = document.getElementById(stationsProjet[i]);
						if(station==null){ console.log("Impossible de trouver la station d'ID " + stationsProjet[i]); }
						else{ tempStations = tempStations.concat('<a onclick="clicked(&quot;' + station.getAttribute('id') + '&quot;);" class="infobulleLink">' + station.getAttribute('value').split(' (')[0] + '</a>'); }
					}
					tempStations = tempStations.concat(')');
				}
				infobulleBigSub.innerHTML = tempStations;
			}
			else if(stationsProjet != null){
				tempStations = (stationsProjet.length == 1 ? "Desservira la station " : "Desservira les stations ");
				for(var i = 0; i < stationsProjet.length; ++i){
					if(i > 0){
						if(i + 1 < stationsProjet.length){ tempStations = tempStations.concat(', '); }
						else{ tempStations = tempStations.concat(' et '); }
					}
					var station = document.getElementById(stationsProjet[i]);
					if(station==null){ console.log("Impossible de trouver la station d'ID " + stationsProjet[i]); }
					else{ tempStations = tempStations.concat('<a onclick="clicked(&quot;' + station.getAttribute('id') + '&quot;);" class="infobulleLink">' + station.getAttribute('value').split(' (')[0] + '</a>'); }
				}
				infobulleBigSub.innerHTML = tempStations;
			}
			else{ infobulleBigSub.innerHTML = "Aucune station desservie. C'est plutôt étrange."; }
	}
	else if(element.classList.contains('stationCable') ||
			element.classList.contains('stationMetro') ||
			element.classList.contains('stationBateau') ||
			element.classList.contains('stationDirigeable') ||
			element.classList.contains('stationRER')){
			// Si on est une station, on affiche en grand les lignes qui y passent et en petit les éventuelles stations reliées
			if(lignes != null){
				tempLignes = (lignes.length == 1 ? "Desservie par le " : "Desservie par les lignes ");
				for(var i = 0; i < lignes.length; ++i){
					if(i > 0){
						if(i + 1 < lignes.length){ tempLignes = tempLignes.concat(', '); }
						else{ tempLignes = tempLignes.concat(' et '); }
					}
					var ligne = document.getElementById(lignes[i]);
					if(ligne==null){ console.log("Impossible de trouver la ligne d'ID " + lignes[i]); }
					else{ tempLignes = tempLignes.concat('<a onclick="clicked(&quot;' + ligne.getAttribute('id') + '&quot;);" class="infobulleLink">' + ligne.getAttribute('id-ligne') + '</a>'); } // En attendant mieux
				}
				if(lignesProjet != null){
					tempLignes = tempLignes.concat(" (en projet : ");
					for(var i = 0; i < lignesProjet.length; ++i){
						if(i > 0){
							if(i + 1 < lignesProjet.length){ tempLignes = tempLignes.concat(', '); }
							else{ tempLignes = tempLignes.concat(' et '); }
						}
						var ligne = document.getElementById(lignesProjet[i]);
						if(ligne==null){ console.log("Impossible de trouver la ligne d'ID " + lignesProjet[i]); }
						else{ tempLignes = tempLignes.concat('<a onclick="clicked(&quot;' + ligne.getAttribute('id') + '&quot;);" class="infobulleLink">' + ligne.getAttribute('id-ligne') + '</a>'); } // En attendant mieux
						tempLignes = tempLignes.concat(')');
					}
				}
				infobulleBigSub.innerHTML = tempLignes;
			}
			else if(lignesProjet != null){
				tempLignes = (lignesProjet.length == 1 ? "Future desserte par le " : "Future desserte par les lignes ");
				for(var i = 0; i < lignesProjet.length; ++i){
					if(i > 0){
						if(i + 1 < lignesProjet.length){ tempLignes = tempLignes.concat(', '); }
						else{ tempLignes = tempLignes.concat(' et '); }
					}
					var ligne = document.getElementById(lignesProjet[i]);
					if(ligne==null){ console.log("Impossible de trouver la ligne d'ID " + lignesProjet[i]); }
					else{ tempLignes = tempLignes.concat('<a onclick="clicked(&quot;' + ligne.getAttribute('id') + '&quot;);" class="infobulleLink">' + ligne.getAttribute('id-ligne') + '</a>'); } // En attendant mieux
				}
				infobulleBigSub.innerHTML = tempLignes;
			}else{ infobulleBigSub.innerHTML = 'Aucune ligne ne passe ici.'; }
			if(stations != null){
				tempStations = "Accès intérieur à ";
				for(var i = 0; i < stations.length; ++i){
					if(i > 0){
						if(i + 1 < stations.length){ tempStations = tempStations.concat(', '); }
						else{ tempStations = tempStations.concat(' et '); }
					}
					var station = document.getElementById(stations[i]);
					if(station==null){ console.log("Impossible de trouver la station d'ID " + stations[i]); }
					else{ tempStations = tempStations.concat('<a onclick="clicked(&quot;' + station.getAttribute('id') + '&quot;);" class="infobulleLink">' + station.getAttribute('value')/*.split(' (')[0]*/ + '</a>'); }
				}
				if(stationsProjet != null){
					tempStations = tempStations.concat(" (en projet : ");
					for(var i = 0; i < stationsProjet.length; ++i){
						if(i > 0){
							if(i + 1 < stationsProjet.length){ tempStations = tempStations.concat(', '); }
							else{ tempStations = tempStations.concat(' et '); }
						}
						var station = document.getElementById(stationsProjet[i]);
						if(station==null){ console.log("Impossible de trouver la station d'ID " + stationsProjet[i]); }
						else{ tempStations = tempStations.concat('<a onclick="clicked(&quot;' + station.getAttribute('id') + '&quot;);" class="infobulleLink">' + station.getAttribute('value')/*.split(' (')[0]*/ + '</a>'); }
					}
					tempStations.concat(")");
				}
				infobulleSmallSub.innerHTML = tempStations;
			}
			else if(stationsProjet != null){
				tempStations = "Projet d'accès intérieur vers ";
				for(var i = 0; i < stationsProjet.length; ++i){
					if(i > 0){
						if(i + 1 < stationsProjet.length){ tempStations = tempStations.concat(', '); }
						else{ tempStations = tempStations.concat(' et '); }
					}
					var station = document.getElementById(stationsProjet[i]);
					if(station==null){ console.log("Impossible de trouver la station d'ID " + stationsProjet[i]); }
					else{ tempStations = tempStations.concat('<a onclick="clicked(&quot;' + station.getAttribute('id') + '&quot;);" class="infobulleLink">' + station.getAttribute('value')/*.split(' (')[0]*/ + '</a>'); }
				}
				infobulleSmallSub.innerHTML = tempStations;
			}
			else{ infobulleSmallSub.innerHTML = ""; }
	}
	else if(element.classList.contains('region')){
			// Si on est une région, on affiche en grand les villes et en petit les éventuelles régions dirigées / dirigeant
			var ville = (villes==null ? null : document.getElementById(villes[0]));
			if(ville == null){ villes = null; }
			if(villes != null){
				tempVilles = 'Capitale: <a onclick="clicked(&quot;' + ville.getAttribute('id') + '&quot;);" class="infobulleLink">' + ville.getAttribute('value') + '</a>' + (villes.length > 1 ? '. Autres villes: ' : '.');
				for(var i = 1; i < villes.length; ++i){
					if(i > 1){
						if(i + 1 < villes.length){ tempVilles = tempVilles.concat(', '); }
						else{ tempVilles = tempVilles.concat(' et '); }
					}
					
					ville = document.getElementById(villes[i]);
					if(ville==null){ console.log("Impossible de trouver la ville d'ID " + villes[i]); }
					else{ tempVilles = tempVilles.concat('<a onclick="clicked(&quot;' + ville.getAttribute('id') + '&quot;);" class="infobulleLink">' + ville.getAttribute('value').split(' (')[0] + '</a>'); }
				}
				infobulleBigSub.innerHTML = tempVilles;
			}else{ infobulleBigSub.innerHTML = "Cette région ne comporte pas de ville."; }
			
			var region = (regions==null ? null : document.getElementById(regions[0]));
			if(region == null){ regions = null; }
			if(regions != null){
				tempRegions = (element.getAttribute('capitale')=='true' ? "Contrôle " : "Contrôlé par ");
				for(var i = 0; i < regions.length; ++i){
					if(i > 0){
						if(i + 1 < regions.length){ tempRegions = tempRegions.concat(', '); }
						else{ tempRegions = tempRegions.concat(' et '); }
					}
					var region = document.getElementById(regions[i]);
					if(region==null){ console.log("Impossible de trouver la région d'ID " + regions[i]); }
					else{ tempRegions = tempRegions.concat('<a onclick="clicked(&quot;' + region.getAttribute('id') + '&quot;);" class="infobulleLink">' + region.getAttribute('value').split(' (')[0] + '</a>'); }
				}
				infobulleSmallSub.innerHTML = tempRegions;
			}
			else{ infobulleSmallSub.innerHTML = ""; }
	}
	else if(element.classList.contains('ville') ||
			element.classList.contains('lieuDit')){
			// Si on est une ville ou un lieu-dit,
			// on affiche la ville à laquelle on appartient si elle existe (si on est un quartier)
			if(villes != null){
				var ville = document.getElementById(villes[0]);
				if(ville == null){ console.log("Impossible de trouver la ville d'ID " + villes[0]); }
				else{ infobulleBigSub.innerHTML = 'Quartier de <a onclick="clicked(&quot;' + ville.getAttribute('id') + '&quot;);" class="infobulleLink">' + ville.getAttribute('value') + '</a>.';
					bigSubtitleTaken = true;}
			} // On appartient également à une région y compris si on est un quartier d'une ville
			if(regions != null){
				var region = document.getElementById(regions[0]);
				if(region == null){ console.log("Impossible de trouver la région d'ID " + regions[0]); }
				else{
					var tempText = (element.getAttribute('capitale') == "true" ? 'Capitale de ' : 'Appartient à ') + '<a onclick="clicked(&quot;' + region.getAttribute('id') + '&quot;);" class="infobulleLink">' + region.getAttribute('value') + '</a>.';
					if(bigSubtitleTaken){		 infobulleSmallSub.innerHTML = tempText; }
					else{ smallSubtitleTaken=true; infobulleBigSub.innerHTML = tempText; }
				}
			}
			else{	if(!smallSubtitleTaken){ infobulleSmallSub.innerHTML = ""; }
					if(!bigSubtitleTaken){ infobulleBigSub.innerHTML = ""; } }
	}
	else if(element.classList.contains('commerce')){
			// Si on est un commerce, on affiche soit la ville soit la région d'appartenance
			if(villes != null){
				var ville = document.getElementById(villes[0]);
				if(ville == null){ console.log("Impossible de trouver la ville d'ID " + villes[0]); }
				else{ infobulleBigSub.innerHTML = 'Commerce de <a onclick="clicked(&quot;' + ville.getAttribute('id') + '&quot;);" class="infobulleLink">' + ville.getAttribute('value') + '</a>.';
					bigSubtitleTaken = true;}
			}
			else if(regions != null){
				var region = document.getElementById(regions[0]);
				if(region == null){ console.log("Impossible de trouver la région d'ID " + regions[0]); }
				else{
					var tempText = (element.getAttribute('capitale') == "true" ? 'Capitale de ' : 'Appartient à ') + '<a onclick="clicked(&quot;' + region.getAttribute('id') + '&quot;);" class="infobulleLink">' + region.getAttribute('value') + '</a>.';
					if(bigSubtitleTaken){		 infobulleSmallSub.innerHTML = tempText; }
					else{ smallSubtitleTaken=true; infobulleBigSub.innerHTML = tempText; }
				}
			}
			else{	if(!smallSubtitleTaken){ infobulleSmallSub.innerHTML = ""; }
					if(!bigSubtitleTaken){ infobulleBigSub.innerHTML = ""; } }
	}
}
function getWikiImage(id){
	if(miniatures[id]!=null){
		miniatureLayer.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', miniatures[id]);
	}
	else{
		miniatureLayer.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', '');
	}
}

/* Fonctions de sélection/désélection d'un élément */
function unclicked(){
	hideInfobulle();
	// console.log('unclicked');
	/* On réinitialise d'abord les éléments principaux cliqués */
	var clickedElems = document.querySelectorAll('.clicked');
	for(var i = 0; i < clickedElems.length; ++i){
		clickedElems[i].classList.remove('clicked');
	}
	/* On réinitialise également les éléments secondaires (non cliqués mais relatifs au calque actuellement cliqué) */
	var relatedElems = document.querySelectorAll('.related');
	for(var i = 0; i < relatedElems.length; ++i){
		relatedElems[i].classList.remove('related');
	}
}
function clicked(layerID){	
	console.log('clicked '+layerID+'');
	clickPosition();
	var layer = document.getElementById(layerID);
	if(layer == null){
		console.log("Impossible de cliquer sur un calque inexistant (" + layerID + ").");
		return;
	}
	var relatedLayers;
	
	/* On désélectionne le calque précédemment cliqué */
	unclicked();
	
	/* Si on n'a pas les bons paramètres de légende pour afficher le calque sélectionné,
		on active les bons paramètres */
	// On affiche le calque TCH si on a sélectionné une ligne ou une station, et on le cache dans tous les autres cas
	if((layer.classList.contains('ligneCable') || layer.classList.contains('ligneMetro') || layer.classList.contains('ligneRER') || layer.classList.contains('ligneBateau') || layer.classList.contains('ligneDirigeable') || layer.classList.contains('stationCable') || layer.classList.contains('stationMetro') || layer.classList.contains('stationRER') || layer.classList.contains('stationBateau') || layer.classList.contains('stationDirigeable'))){ if(!metroShown){ toggleLayer('calqueTCH'); }}
	else{ if(metroShown){ toggleLayer('calqueTCH'); } }
	// On affiche le calque des régions si on a sélectionné une région
	if(layer.classList.contains('region')){ if(!regionsShown){ toggleLayer('calqueRegions'); }}
	else if(layer.parentNode.classList.contains('geographie')){ if(regionsShown){ toggleLayer('calqueRegions'); } }
	if(layer.classList.contains('projet') && !projetsShown){ toggleLayer('calqueProjets'); }
	if(layer.classList.contains('ville') && !villesShown){ toggleLayer('calqueVilles'); }
	if(layer.classList.contains('route') && !routesShown){ toggleLayer('calqueRoutes'); }
	if(layer.classList.contains('lieuDit') && !lieuxDitsShown){ toggleLayer('calqueLieuxDits'); }
	if(layer.classList.contains('commerce') && !commercesShown){ toggleLayer('calqueCommerces'); }
	if(layer.parentNode.classList.contains('geographie') && !geoShown){ toggleLayer('calqueGeographie'); }
	if((layer.classList.contains('fleuve') || layer.classList.contains('riviere'))
		&& !fleuvesShown){ toggleLayer('calqueCoursEau'); }
	
	/* On ajoute la classe correspondante au calque nouvellement sélectionné */
	layer.classList.add('clicked');
	/* Si ce calque inclut des éléments reliés, on leur applique également l'effet visuel */
	if(relatedLayers = layer.getAttribute('related')){
		relatedLayers = relatedLayers.split(' ');
		for(var i = 0; i < relatedLayers.length; ++i){
			var relatedLayer = document.getElementById(relatedLayers[i]);
			if(relatedLayer!=null){ relatedLayer.classList.add('related'); }
		}
	}
	
	/* On affiche les infos dans l'infobar */
	showInfobulle(document.getElementById(layerID));
	
	
	if(Date.now() - lastStillClick < 500 && selectedElement.getAttribute('id') == layerID){
		centerView();
	}
	else{
		selectedElement = document.getElementById(layerID);
	}
	
	lastStillClick = Date.now();
}



function addLetter(key){
	rechercheTexte.innerHTML = rechercheTexte.innerHTML.slice(0, rechercheCurseurPos) + key + rechercheTexte.innerHTML.slice(rechercheCurseurPos);
	cursorToRight();
}
function removePreviousLetter(){
	if(rechercheCurseurPos <= 0){return false;}
	rechercheTexte.innerHTML = rechercheTexte.innerHTML.slice(0, rechercheCurseurPos - 1) + rechercheTexte.innerHTML.slice(rechercheCurseurPos);
	cursorToLeft();
}
function removeNextLetter(){
	if(rechercheCurseurPos >= rechercheTexte.innerHTML.length){return;}
	rechercheTexte.innerHTML = rechercheTexte.innerHTML.slice(0, rechercheCurseurPos) + rechercheTexte.innerHTML.slice(rechercheCurseurPos + 1);
}
function resetLetters(){
	rechercheCurseurPos = 0;
	rechercheTexte.innerHTML = '';
}

function cursorToLeft(){
	if(rechercheCurseurPos > 0) { --rechercheCurseurPos; }
	refreshCursor();
}
function cursorToRight(){
	if(rechercheCurseurPos < rechercheTexte.innerHTML.length) { ++rechercheCurseurPos; }
	refreshCursor();
}
function cursorToStart(){
	rechercheCurseurPos = 0;
	refreshCursor();
}
function cursorToEnd(){
	rechercheCurseurPos = rechercheTexte.innerHTML.length;
	refreshCursor();
}
function refreshCursor(){
	if(rechercheCurseurPos == 0) rechercheCurseur.setAttribute('x', rechercheTexte.getAttribute('x'));
	else rechercheCurseur.setAttribute('x', rechercheTexte.getEndPositionOfChar(rechercheCurseurPos - 1).x);
}

function moveUp(createInterval){
	createInterval = (typeof createInterval !== 'undefined') ? createInterval : false;
	offsetY -= (sizeY / 50);
	rescale();
	if(createInterval) moveUD = setInterval(moveUp, 25);
}
function moveDown(createInterval){
	createInterval = (typeof createInterval !== 'undefined') ? createInterval : false;
	offsetY += (sizeY / 50);
	rescale();
	if(createInterval) moveUD = setInterval(moveDown, 25);
}
function moveLeft(createInterval){
	createInterval = (typeof createInterval !== 'undefined') ? createInterval : false;
	offsetX -= (sizeX / 60);
	rescale();
	if(createInterval) moveLR = setInterval(moveLeft, 25);
}
function moveRight(createInterval){
	createInterval = (typeof createInterval !== 'undefined') ? createInterval : false;
	offsetX += (sizeX / 60);
	rescale();
	if(createInterval) moveLR = setInterval(moveRight, 25);
}

function standardizeText(text){
	if(text == null){ return ''; }
	return text.toLowerCase().replace(/[ÀÁÂÃÄÅàáâãäå]/g, 'a').replace(/[Ææ]/g, 'ae').replace(/[çÇ]/g, 'c').replace(/[ÈÉÊËèéêë]/g, 'e').replace(/[ÌÍÎÏìíîï]/g, 'i').replace(/[Ðð]/g, 'd').replace(/[Ññ]/g, 'n').replace(/[ÒÓÔÕÖØòóôõöø]/g, 'o').replace(/[ÙÚÛÜ]/g, 'u').replace(/[Ýýÿ]/g, 'y').replace(/[Þþ]/g, 'th').replace(/ß/g, 'ss');
}

function displayResult(id){	
	if(rechercheShown){ toggleRecherche(); }
	clicked(id,1);
	clicked(id,1);
}
function displayNoResults(){
	displayResultsList();	// On appelle la fonction sans argument pour vider la liste précédente
	// On affiche ensuite un message indiquant l'absence de résultats
	var errorElem = rechercheResultats.appendChild(document.createElementNS("http://www.w3.org/2000/svg", "text"));
	errorElem.setAttribute('x', parseFloat(document.getElementById('rechercheResultatsTitre').getAttribute('x')) + 15);
	errorElem.setAttribute('y', parseFloat(document.getElementById('rechercheResultatsTitre').getAttribute('y')) + 25);
	errorElem.setAttribute('id', 'rechercheSansResultat');
	errorElem.innerHTML = 'Pas de résultats...';
}
function displayResultsList(results){
	if (rechercheShown == 0) {
		rechercheLayer.style.animationName = 'rechercheAnim_noneToBig';
		rechercheLayer.style.animationPlayState = 'running'; }
	else{
		rechercheLayer.style.animationName = 'rechercheAnim_smallToBig';
		rechercheLayer.style.animationPlayState = 'running'; }
	rechercheShown = 2;
	
	// On nettoie d'abord les anciens résultats
	while(rechercheResultats.firstChild){ rechercheResultats.removeChild(rechercheResultats.lastChild); }
	scrollResultatsRecherche();
	
	// S'il n'y a pas de résultat, on arrête la fonction ici
	if(!results){ return; }
	
	//searchResultsOffsetMax = (results.length < 10 ? 0 : 250 * (results.length - 10));
	//rechercheTexte.innerHTML = '';
	results.sort(function(a,b){return b.value - a.value;});
	var beginX = parseFloat(document.getElementById('rechercheResultatsTitre').getAttribute('x')) + 15;
	var beginY = parseFloat(document.getElementById('rechercheResultatsTitre').getAttribute('y')) + 25;
	for(var elem of results){
		var imgElem = rechercheResultats.appendChild(document.createElementNS("http://www.w3.org/2000/svg", "image"));
		imgElem.setAttribute('y', beginY - 15);
		imgElem.setAttribute('x', beginX - 10);
		imgElem.setAttribute('width', 16);
		imgElem.setAttribute('height', 16);
		imgElem.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', 'icon/livre.svg');
		imgElem.classList.add('interfaceHoverable');
		imgElem.setAttribute('hoverInfo', 'Accéder à "' + elem.name + '" sur le site du CGC.');
		addHoverInfo(imgElem);
		imgElem.setAttribute('onclick', 'window.open("https://cgc.terresduhameau.eu.org/wiki/' + elem.name + '");');
		
		var textElem = rechercheResultats.appendChild(document.createElementNS("http://www.w3.org/2000/svg", "text"));
		textElem.setAttribute('y', beginY);
		textElem.classList.add('searchResult');
		textElem.classList.add('interfaceHoverable');
		textElem.setAttribute('hoverInfo', 'Sélectionner "' + elem.name + '" sur la carte.');
		addHoverInfo(textElem);
		textElem.innerHTML = '· ' + elem.name;
		textElem.setAttribute('x', beginX);
		textElem.setAttribute('value', elem.id);
		textElem.setAttribute('onclick', 'clicked("' + elem.id + '");');
		
		beginY += 20;
	}
	rechercheResultatsMaxOffset = Math.max(0, 50 + (results.length * 20) - parseFloat(document.getElementById('rechercheResultatsBg').getAttribute('height')));
}
function scrollResultatsRecherche(e){
	// Si on appelle la fonction sans argument, on remet le scroll à 0
	var delta;
	if(!e){ delta = 0; }
	else{ delta = -e.deltaY; e.preventDefault(); }
	
	if(delta > 0) { rechercheResultatsOffset = Math.max(0, rechercheResultatsOffset - (e.shiftKey ? 187 : 37)); }
	else if(delta < 0) { rechercheResultatsOffset = Math.min(rechercheResultatsMaxOffset, rechercheResultatsOffset + (e.shiftKey ? 187 : 37)); }
	else{ rechercheResultatsOffset = 0; }
	rechercheResultats.setAttribute('transform', 'translate(0 ' + (-rechercheResultatsOffset) + ')');
	document.getElementById('rechercheResultatsClipRect').setAttribute('transform', 'translate(0 ' + rechercheResultatsOffset + ')');
	
	return false;
}


function displaySearchResult(){
	if(rechercheTexte.innerHTML.length == 0){return;}
	
	var searchStr = rechercheTexte.innerHTML.split(' ');
	console.log(searchStr);
	var list = document.getElementsByClassName('hoverable');
	
	var results = [];
	var currentResult = -1;
	
	for(var elem of list){
		var resultSet = false;
		for(var str of searchStr){
			if(str=='' || str==' ') { continue; }
			str = standardizeText(str);
			if(!elem.getAttribute('value')){console.log(elem.id);}
			if(	standardizeText(elem.id).indexOf(str) != -1 ||
				standardizeText(elem.getAttribute('value')).indexOf(str) != -1){
				if(!resultSet){
					resultSet=true;
					++currentResult;
					results.push({id:elem.id,name:elem.getAttribute('value'),value:1});
				}
				else{
					++results[currentResult].value;
				}
			}
		}
	}
	
	if(results.length == 0){ displayNoResults(); }
	else if(results.length == 1){displayResult(results[0].id);}
	else{displayResultsList(results);}
}


function keyDown(e){	
	if(e.ctrlKey){return;}

	switch(e.key){
		case 'ArrowLeft':
			if(rechercheFocus) cursorToLeft();
			else if(moveLR == null) moveLeft(true);
			break;
		case 'ArrowRight':
			if(rechercheFocus) cursorToRight();
			else if(moveLR == null)	moveRight(true);
			break;
		case 'ArrowUp':
			if(!rechercheFocus && moveUD == null) moveUp(true);
			break;
		case 'ArrowDown':
			if(!rechercheFocus && moveUD == null) moveDown(true);
			break;
		default:
			if(!rechercheFocus) break;
			if((e.key.length == 1) &&
			  ((e.key >= 'a' && e.key <= 'z') ||
			   (e.key >= 'A' && e.key <= 'Z') ||
			   (e.key >= 'À' && e.key <= 'ÿ') ||
			   (e.key >= '0' && e.key <= '9') ||
			   (e.key == '\'' || e.key == '(' || e.key == ')' || e.key == '/' || e.key == '\\' || e.key == ' '))) {
				addLetter(e.key);
			}
			else if(e.key == 'Backspace'){
				removePreviousLetter();
			}
			else if(e.key == 'Delete'){
				if(e.shiftKey){resetLetters();}
				else{removeNextLetter();}
			}
			else if(e.key == 'Home' || e.key == 'PageUp'){
				cursorToStart();
			}
			else if(e.key == 'End' || e.key == 'PageDown'){
				cursorToEnd();
			}
			else if(e.key == 'Enter'){
				displaySearchResult();
			}
	}
	
	e.preventDefault();
	e.stopPropagation();
	return false;
}

function keyUp(e){
	if(e.ctrlKey || rechercheFocus){
		return;
	}
	
	switch(e.key){
		case 'ArrowLeft':
		case 'ArrowRight':
			if(moveLR != null){
				clearInterval(moveLR);
				moveLR = null;
			} break;
		case 'ArrowUp':
		case 'ArrowDown':
			if(moveUD != null){
				clearInterval(moveUD);
				moveUD = null;
			} break;
		case 'C': case 'c':
			toggleLayer('calqueRoutes');
			break;
		case 'F': case 'f':
			toggleRecherche();
			break;
		case 'G': case 'g':
			toggleLayer('grille');
			break;
		case 'L': case 'l':
			toggleLayer('calqueLieuxDits');
			break;
		case 'M': case 'm':
			toggleLayer('calqueTCH');
			break;
		case 'N': case 'n':
			toggleLayer('calqueGeographie');
			break;
		case 'P': case 'p':
			toggleLayer('calqueProjets');
			break;
		case 'R': case 'r':
			toggleLayer('calqueRegions');
			break;
		case 'T': case 't':
			toggleLayer('calqueTopographie');
			if(topoShown){
				topoSecondaryBg.style.display='inline';
			}
			else{
				topoSecondaryBg.style.display='none';
			}
			break;
		case 'V': case 'v':
			toggleLayer('calqueVilles');
			break;
		case 'W': case 'w':
			toggleLayer('calqueCoursEau');
			break;
		case 'X': case 'x':
			toggleLayer('positionnement');
			break;
		case '*':
			doubleResolution = !doubleResolution;
			break;
		case '+':
			if(grilleShown && grilleSpacing < 1024){
				grilleSpacing *= 2;
				initGrille();
			}
			break;
		case '-':
			if(grilleShown && grilleSpacing > 16){
				grilleSpacing /= 2;
				initGrille();
			}
	}
	
	e.preventDefault();
	e.stopPropagation();
	return false;
}

function keyPress(e){
	e.preventDefault();
	e.stopPropagation();
	return false;
}


function centerView(current){
	current = (typeof current !== 'undefined') ? current : selectedElement;
	
	if(current != null){
		var currentBB = current.getBBox();
		var x = currentBB.x + (currentBB.width / 2);
		var y = currentBB.y + (currentBB.height / 2);
		var sx = (currentBB.width * (sizeY / sizeX) > currentBB.height) ? currentBB.width : currentBB.height * (sizeX / sizeY);
		viewboxPosition(x, y, sx * 1.5);
	}
}

function hideSplash(){
	// On cache le splash
	document.getElementById('loading').style.display='none';
	// On affiche le texte d'introduction
	// document.getElementById('helpPopup').style.animationPlayState='running';
}

function testForTileHeightChange(){
	var previousTileHeight = displayedTileHeight;
	computeTileHeight();
	if(previousTileHeight == displayedTileHeight){ return; }
	
	mapBitIndex = 0;
	loadMaps();
	topoBitIndex = 0;
	loadTopo();
}

function computeTileHeight(){
	var map = mapBits[0];
	if(!map) { return; }
	
	computedTileHeight = map.getBoundingClientRect().height;
	displayedTileHeight = 1024;
	while(displayedTileHeight > computedTileHeight && displayedTileHeight >= 128){
		displayedTileHeight /= 2;
	}
	
	if(doubleResolution && displayedTileHeight < 1024){
		displayedTileHeight *= 2;
	}
}

function loadMaps(){
	for(var i = 0; mapBitIndex < mapBits.length; ++mapBitIndex){ ++i;
		if(mapBits[mapBitIndex] == undefined){console.log(mapBitIndex + ' is undefined');continue;}
		mapBits[mapBitIndex].setAttributeNS('http://www.w3.org/1999/xlink', 'href', 'maps/' + Math.round(displayedTileHeight) + '/tile_' + mapBits[mapBitIndex].getAttribute('value') + '.png');
		if(i>32){
			document.getElementById('loadingSubtitle').innerHTML = '' + mapBitIndex + ' chargées sur ' + mapBits.length;
			setTimeout(loadMaps, 10);
			return; }
	}
}

function loadTopo(){
	for(var i = 0; topoBitIndex < topoBits.length; ++topoBitIndex){ ++i;
		if(topoBits[topoBitIndex] == undefined){console.log(topoBitIndex + ' is undefined');continue;}
		if(displayedTileHeight <= 512){
			topoBits[topoBitIndex].setAttributeNS('http://www.w3.org/1999/xlink', 'href', 'topo/' + Math.round(displayedTileHeight) + '/' + topoBits[topoBitIndex].getAttribute('value') + '.png');
		}
		else{
			topoBits[topoBitIndex].setAttributeNS('http://www.w3.org/1999/xlink', 'href', 'topo/512/' + topoBits[topoBitIndex].getAttribute('value') + '.png');
		}
		if(i>32){
			document.getElementById('loadingSubtitle').innerHTML = '' + topoBitIndex + ' chargées sur ' + topoBits.length;
			setTimeout(loadTopo, 10);
			return; }
	}
}

function fillWindow(){
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;
	var widthOffset = (windowWidth - defaultWidth);
	var heightOffset = (windowHeight - defaultHeight);
	
	svg.setAttribute('width', windowWidth);
	svg.setAttribute('height', windowHeight);
	svg.setAttribute('viewBox', '0 0 ' + windowWidth + ' ' + windowHeight);
	
	// On redimensionne la carte et l'interface
	mapParent.setAttribute('width', windowWidth);
	mapParent.setAttribute('height', windowHeight - 100);
	mapParent.setAttribute('viewBox', '' + defaultViewbox[0] + ' ' + defaultViewbox[1] + ' ' + (defaultViewbox[2] + (widthOffset * defaultViewbox[2] / windowWidth)) + ' ' + defaultViewbox[3]);
	
	document.getElementById('loading').setAttribute('transform', 'translate(' + (widthOffset / 2) + ' ' + (heightOffset / 2) + ')');
	document.getElementById('infoBar').setAttribute('transform', 'translate(0 ' + heightOffset + ')');
	document.getElementById('infobarMiniature').setAttribute('transform', 'translate(' + widthOffset + ' 0)');
	document.getElementById('rechercheTransform').setAttribute('transform', 'translate(' + widthOffset + ' 0)');
	var clipPath = document.getElementById('infoBarRect');
	clipPath.setAttribute('width', windowWidth - 360);
}

function onLoaded(){
	mapParent = document.getElementById('carte');
	pt = mapParent.createSVGPoint();
	
	defaultWidth = svg.getAttribute('width');
	defaultHeight = svg.getAttribute('height');
	defaultViewbox = mapParent.getAttribute('viewBox').split(' ');
	for(var i = 0; i < defaultViewbox.length; ++i){ defaultViewbox[i] = parseFloat(defaultViewbox[i]); }
	
	legende = document.getElementById('legende');
	legendeFleche = document.getElementById('legendeFleche');
	infobulle = document.getElementById('infobulle');
	infobullePin = document.getElementById('infobullePin');
	infobulleLink = document.getElementById('infobulleLink');
	infobulleText = document.getElementById('infobulleText');
	infobulleSmallSub = document.getElementById('infobulleSmallSubtitle');
	infobulleBigSub = document.getElementById('infobulleBigSubtitle');
	
	interfaceLayer = document.getElementById('interface');
	miniatureLayer = document.getElementById('miniature');
	rechercheLayer = document.getElementById('recherche');
	rechercheResultats = document.getElementById('rechercheResultats');
	rechercheTexte = document.getElementById('rechercheChampTexte');
	rechercheCurseur = document.getElementById('rechercheCurseur');
	
	regionsLayer = document.getElementById('calqueRegions');
	tchLayer = document.getElementById('calqueTCH');
	routesLayer = document.getElementById('calqueRoutes');
	fleuvesLayer = document.getElementById('calqueCoursEau');
	villesLayer = document.getElementById('calqueVilles');
	commercesLayer = document.getElementById('calqueCommerces');
	lieuxDitsLayer = document.getElementById('calqueLieuxDits');
	grilleLayer = document.getElementById('grille');
	projetsLayer = document.getElementById('calqueProjets');
	geoLayer = document.getElementById('calqueGeographie');
	topoLayer = document.getElementById('calqueTopographie');
	pinLayer = document.getElementById('positionnement');
	coordsLayer = document.getElementById('coordonnees');
	coordsText = document.getElementById('coordsText');
	mapIDText = document.getElementById('mapIDText');
	hoverInfoLayer = document.getElementById('hoverInfo');
	hoverInfoRect = document.getElementById('hoverInfoBg');
	hoverInfoText = document.getElementById('hoverInfoText');
	
	fillWindow();
	
	grilleHoriz = document.getElementById('grilleHorizontal');
	grilleVert = document.getElementById('grilleVertical');
	
	smallPaths = document.getElementsByClassName('smallPath');
	mediumPaths = document.getElementsByClassName('mediumPath');
	bigPaths = document.getElementsByClassName('bigPath');
	
	mapBits = document.getElementsByClassName('mapBit');
	topoBits = document.getElementsByClassName('topoBit');
	
	document.getElementById('loadingText').innerHTML = 'Chargement des cartes...';
	setTimeout(onLoaded2, 50);
}
function onLoaded2(){
	computeTileHeight();
	loadMaps();
	setTimeout(onLoaded3, 50);
}
function onLoaded3(){
	if(mapBitIndex < mapBits.length){ setTimeout(onLoaded3, 100); }
	else{
		document.getElementById('loadingText').innerHTML = 'Chargement de la topographie...';
		document.getElementById('loadingSubtitle').innerHTML = '';
		loadTopo();
		setTimeout(onLoaded4, 50); }
}
function onLoaded4(){
	if(topoBitIndex < topoBits.length){ setTimeout(onLoaded4, 100); }
	else{
		document.getElementById('loadingText').innerHTML = 'Finalisation...';
		document.getElementById('loadingSubtitle').innerHTML = '';
		setTimeout(onLoaded5, 50); }
}
function onLoaded5(){
	updateViewbox();
	posFrom = document.getElementById('posFrom');
	posTo = document.getElementById('posTo');
	mapElement = mapBits[0];
	mapBitOffset = posmod(parseInt(mapElement.getAttribute('x')), 128);
	topoSecondaryBg = document.getElementById('topoSecondaryBg');
	rescale();
	initGrille();
	
	var togglableLayers = document.getElementsByClassName('mainLayer');
	var currentX = parseFloat(document.getElementById('legendeTitre').getAttribute('x'));
	var currentY = parseFloat(document.getElementById('legendeTitre').getAttribute('y')) + 30;
	for(var i = 0, max = togglableLayers.length; i < max; ++i){
		var checkboxElem = legende.appendChild(document.createElementNS("http://www.w3.org/2000/svg", "image"));
		checkboxElem.setAttribute('onclick', 'toggleLayer("' + togglableLayers[i].getAttribute('id') + '")');
		checkboxElem.setAttribute('x', currentX);
		checkboxElem.setAttribute('y', currentY-17);
		checkboxElem.setAttribute('width', 16);
		checkboxElem.setAttribute('height', 16);
		checkboxElem.setAttribute('id', 'legendeToggle_' + togglableLayers[i].getAttribute('id'));
		checkboxElem.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href',
			(isLayerVisible(togglableLayers[i].getAttribute('id')) ? 'icon/checkbox_on.svg' : 'icon/checkbox_off.svg'));
		
		var textElem = legende.appendChild(document.createElementNS("http://www.w3.org/2000/svg", "text"));
		textElem.setAttribute('x', currentX+30);
		textElem.setAttribute('y', currentY);
		currentY += 20;
		textElem.innerHTML = '<a onclick="toggleLayer(&quot;' + togglableLayers[i].getAttribute('id') + '&quot;);" class="infobulleLink">' + togglableLayers[i].getAttribute('value') + '</a>';
	}

	document.addEventListener("keydown", keyDown, {capture:false,passive:false});
	document.addEventListener("keyup", keyUp, {capture:false,passive:false});
	document.addEventListener("keypress", keyPress, {capture:false,passive:false});
	mapParent.addEventListener("mousedown", mapMouseDown, {capture:false,passive:false});
	document.addEventListener("mousedown", globalMouseDown, {capture:false,passive:false});
	mapParent.addEventListener("mouseup", mapMouseUp, {capture:false,passive:false});
	document.addEventListener("mouseleave", mouseLeave, {capture:false,passive:false});
	mapParent.addEventListener("mousemove", mapMouseMove, {capture:false,passive:false});
	interfaceLayer.addEventListener("mousemove", menuMouseMove, {capture:false,passive:false});
	document.addEventListener("contextmenu", function(e){e.preventDefault();}, {capture:false,passive:false});
	mapParent.addEventListener("wheel", mapMouseZoom, {capture:false,passive:false});
	window.addEventListener("resize", fillWindow, {capture:false,passive:false});
	
	document.getElementById('legendeFlecheFg').addEventListener("mousedown", toggleLegende, {capture:false,passive:false});
	document.getElementById('rechercheFlecheFg').addEventListener("mousedown", toggleRecherche, {capture:false,passive:false});
	document.getElementById('rechercheBoutonFg').addEventListener("mousedown", displaySearchResult, {capture:false,passive:false});
	document.getElementById('rechercheResultatsTransform').addEventListener("wheel", scrollResultatsRecherche, {capture:false,passive:false});
	
	var hoverableButtons = document.getElementsByClassName('interfaceHoverable');
	for(var i = 0, max = hoverableButtons.length; i < max; ++i){ addHoverInfo(hoverableButtons[i]); }
	
	document.getElementById('loadingText').innerHTML = 'Chargement terminé !'
	document.getElementById('loading').style.animationPlayState = 'running';
	setTimeout(hideSplash, 1250);
}
